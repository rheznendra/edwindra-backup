<!DOCTYPE html>
<html>

<head>
    <title>Resource || KKP Riza Edwindra</title>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php $site_name = "Resource || KKP Riza Edwindra"; include 'item/css.php'; include("item/open_graph.php"); ?>

</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header navbar-right">
                <div id="nav-icon3" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                </div>
                <a class="navbar-brand" href="index">
                    <img src="public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-menu" style="margin-top:10px;">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about">About</a>
                    </li>
                    <li>
                        <a href="services">Services</a>
                    </li>
                    <li>
                        <a href="client">Client</a>
                    </li>
                    <li>
                        <a href="partner">Partner</a>
                    </li>
                    <li class="active">
                        <a href="resource">Resource</a>
                    </li>
                    <li>
                        <a href="career">Career</a>
                    </li>
                    <li>
                        <a href="article">Article</a>
                    </li>
                    <li>
                        <a href="contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="parallax">
        <div class="container">
            <div class="row">
                <p class="text-center animated fadeInDown about-title" style="">Resource</p>
            </div>
        </div>
    </div>
    <div class="afexion">
        <div class="container">
            <div class="row">
                    <?php
include("admin/content/koneksi.php");
$sql = mysqli_query($connect, "SELECT * FROM resource ORDER BY id DESC");
$row = mysqli_num_rows($sql);
if ($row == 0) {
echo "<div class='error'><h3>Tidak ada data untuk ditampilkan</h3></div>";
} else {
$i = 0;
if($row%2 == 0 ) {
$a = 0;
$wrap_count = round($row/2,0,PHP_ROUND_HALF_DOWN);
} else {
$a = 1;
$wrap_count = round($row/2,0,PHP_ROUND_HALF_UP);
}
while ($data = mysqli_fetch_assoc($sql)) {
$i += 1;
if($row > 3) {
if($i%$wrap_count == 1) {
echo '<div class="col-md-6">';
}
} else if($row > 1 && $row <= 3) {
if($i%$wrap_count == 1){
echo '<div class="col-md-6">';
}
if($i%$wrap_count == 0 && $a == 0){
echo '<div class="col-md-6">';
}
} else {
echo '<div class="col-md-12">';
}
if($data['download_link'] =="") {
    $data['download_link'] = $data['link'];
}
if(strlen($data['description']) > 200) {
$desc = strip_tags(substr($data['description'],0,200));
} else {
$desc = strip_tags(substr($data['description'],0,200));
}
?>
                        <div class="fancy-collapse-panel">
                            <div class="panel-group" id="accordion<?php echo $data['id']; ?>" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default" style="border: none;">
                                    <div class="panel-heading" role="tab" id="heading<?php echo $data['id']; ?>">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion<?php echo $data['id']; ?>" href="#collapse<?php echo $data['id']; ?>" aria-expanded="false" aria-controls="collapse<?php echo $data['id']; ?>">
                                                <?php echo $data['title']; ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?php echo $data['id']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $data['id']; ?>">
                                        <div class="panel-body table-body">
                                            <?php echo $desc; ?>...<a target="_blank" href="<?php echo $data['link']; ?>"> Read More</a>
                                        </div>
                                        <div class="panel-body">
                                            <i class="fa fa-link"></i> <a class="read-more" target="_blank" href="<?php echo $data['download_link'] ?>" style="font-weight:bold;text-decoration:none;"> Download Now...</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
if($row != 1) {
if($i%$wrap_count == 0) {
echo '</div>';
} else if($a == 1 && $i == $row) {
echo '</div>';
}
} else if( $row > 1 && $row <= 3) {
if($i%$wrap_count == 3){
echo '</div>';
}
} else {
echo '</div>';
}
}   }
?>
            </div>
        </div>
    </div>

    <?php
      include 'item/footer.php';
      include 'item/js.php';
    ?>
</body>

</html>
