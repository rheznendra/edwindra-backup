<?php
    if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
        header( 'HTTP/1.0 403 Forbidden', TRUE, 403 );
        die( header( 'location: /index' ) );
        exit();
    } else {
$description = "Kantor Konsultan Pajak Riza Edwindra, established in 2006, and  obtain its registered licence in 2010 Started as a small business, we are now located at Jl. Kecubung no 7, Denpasar Timur, with 9 employees. We are registered tax consultant. We committed on giving services to tax payer who has growing demand of tax administration, tax audit, and tax dispute (tax objection and tax appeal), on acquisition and merger and also tax due diligence";
$url = "https://".$_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$img = "https://edwindra.com/public/img/LOGO-KKP-FOR-WEB-BG-hitam.png";
?>

    <meta name="google-site-verification" content="iWu1TsrKxJGnhoz3IC5Fj-zkJMFWNz7PdHXDOm8bFJM" />
    <meta name="image" content="<?= $img; ?>">
    <meta name="description" content="<?= $description; ?>">
    <meta http-equiv="cache-control" control="no-cache">
    <meta property="og:title" content="<?= $site_name; ?>">
    <meta property="og:url" content="<?= $url; ?>">
    <meta property="og:image" content="<?= $img; ?>">
    <meta property="og:image:alt" content="Logo KKP Riza Edwindra">
    <meta property="og:description" content="<?= $description; ?>">
    <meta property="og:site_name" content="<?= $site_name; ?>">
    <meta property="og:type" content="website">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="<?= $site_name; ?>">
    <meta name="twitter:description" content="Kantor<?= $description; ?>">
    <meta name="twitter:image" content="<?= $img; ?>">
    <meta itemprop="name" content="<?= $site_name; ?>">
    <meta itemprop="description" content="<?= $description; ?>">
    <meta itemprop="image" content="<?= $img; ?>">
<?php } ?>