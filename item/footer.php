<div class="container footer2">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <h2>Contact Info</h2>
            <p style="margin-bottom: 3%; margin-top: 3%">Edwindra & Partner</p>
            <p class="footer-text"><i class="fa fa-map-marker"></i> Jl. Kecubung No 7, Denpasar, Bali 80236
                <br><i class="fa fa-map-marker"></i> Jl. Kelud CI 13 Waru, Sidoarjo, Jawa Timur. 61256</p>
            <p class="footer-text"><i class="fa fa-envelope"></i> Email : info@edwindra.com</p>
            <p class="footer-text"><i class="fa fa-phone"></i> Phone : +62 361 445 7320 / +62 816 471 1418</p>
        </div>
        <div class="col-md-8 col-xs-12 footer-content">
            <h2 style="margin-bottom: 5%" class="text-left">Resource Link</h2>
            <div class="row">
                <div class="col-md-4 col-xs-4">
                    <a href="http://www.pajak.go.id/" target="_blank">
                        <img src="<?php echo "https://" . $_SERVER['SERVER_NAME'].""; ?>/public/img/dirjen-pajak.png" class="img-responsive img-footer" width="40%" alt="dirjen pajak">
                    </a>
                </div>
                <div class="col-md-4 col-xs-4">
                    <a href="http://www.ikpi.or.id/" target="_blank">
                        <img src="<?php echo "https://" . $_SERVER['SERVER_NAME'].""; ?>/public/img/ikpi.png" class="img-responsive img-footer" width="40%" alt="ikatan konsultan pajak indonesia">
                    </a>
                </div>
                <div class="col-md-4 col-xs-4">
                    <a href="http://iaiglobal.or.id" target="_blank">
                        <img src="<?php echo "https://" . $_SERVER['SERVER_NAME'].""; ?>/public/img/iai.png" class="img-responsive img-footer" width="40%" alt="ikatan akuntan indonesia">
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-12 text-left">
            <hr style="margin-top: 30px;">
            <p style="font-size: 12px;">&copy; 2017 KKP Riza Edwindra, All Rights Reserved. Design and Photos By Laksmaputra</p>
        </div>
    </div>
</div>