<!DOCTYPE html>
<html>

<head>
    <title> KKP Riza Edwindra </title>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php $site_name = "KKP Riza Edwindra"; include 'item/css.php'; include("item/open_graph.php"); ?>

</head>

<body>
    <style>
@media only screen and (max-width: 480px) {
    body {
        background-size:inherit;
    }
}
    </style>
    <nav class="navbar navbar-custom2 navbar-fixed-top">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <li>
                    <div id="nav-icon2">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <div class="sliding-menu flex-center-wrapper left-menu">
        <ul class="sliiide-device">
            <li>
                <a href="about">About</a>
            </li>
            <hr>
            <li>
                <a href="services">Services</a>
            </li>
            <hr>
            <li>
                <a href="client">Client</a>
            </li>
            <hr>
            <li>
                <a href="partner">Partner</a>
            </li>
            <hr>
            <li>
                <a href="resource">Resource</a>
            </li>
            <hr>
            <li>
                <a href="career">Career</a>
            </li>
            <hr>
            <li>
                <a href="article">Article</a>
            </li>
            <hr>
            <li>
                <a href="contact">Contact Us</a>
            </li>
        </ul>
    </div>

    <div class="parallax">
        <div class="container">
            <div class="row">
                <center>
                    <img src="public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" alt="" class="img-responsive animated flipInX logo-front"><br/>
                </center>
            </div>
        </div>
    </div>

    <?php include 'item/js.php'; ?>
</body>

</html>