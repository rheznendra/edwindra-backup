<?php
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    header('location:../login.php');
  exit();
} else {
?>
<!DOCTYPE html>
<html>

<head>
    <title>Edit Resource || Admin Edwindra</title>
<?php include('../item/css.php'); ?>
    <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <style>
        th, td {
            text-align: center;
        }
        #table-menu {
            text-align: right;
            width: 20%;
        }

        #table-id {
            width: 5%;
        }
    </style>
    <div class="wrapper">
        <header class="main-header">
            <a href="index" class="logo">
                <span class="logo-mini">
                    <b>E</b>
                </span>
                <span class="logo-lg">
                    <b>Edwindra</b>.com
                </span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </nav>
        </header>

        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header text-center">ADMIN EDWINDRA</li>
                    <li>
                        <a href="index">
                            <i class="fa fa-home"></i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-plus-square"></i>
                            <span>Add Data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="../add/resource">
                                    <i class="fa fa-circle-o"></i> Resource</a>
                            </li>
                            <li>
                                <a href="../add/career">
                                    <i class="fa fa-circle-o"></i> Career</a>
                            </li>
                            <li>
                            <a href="../add/article">
                                    <i class="fa fa-circle-o"></i> Article</a>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview active">
                        <a href="#">
                            <i class="fa fa-edit"></i>
                            <span>Edit Data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="resource">
                                    <i class="fa fa-circle-o"></i> Resource</a>
                            </li>
                            <li>
                                <a href="career">
                                    <i class="fa fa-circle-o"></i> Career</a>
                            </li>
                            <li class="active">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-circle-o"></i> Article</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </section>
        </aside>
        <div class="content-wrapper">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li>
                        <a href="#">
                            <i class="fa fa-home"></i> Home</a>
                    </li>
                    <li>
                        <a href="#">Edit Data</a>
                    </li>
                    <li class="active">Article</li>
                </ol>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="callout callout-info" id="callout-message" style="display:none;"></div>
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Data Article</h3>
                            </div>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>TITLE</th>
                                            <th>CREATOR</th>
                                            <th>UPLOADER</th>
                                            <th>POST DATE</th>
                                            <th>MENU</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
<?php include('../item/footer.php'); ?>

    </div>
<?php include('../item/js.php'); ?>
    <script src="../bower_components/datatables.net/js/jquery.dataTables.min.js "></script>
    <script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js "></script>
    <script src="../item/error.js "></script>
    <script>
        $(document).ready(function () {
            var table = $('#example1').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-id');
                    }
                },{
                    'targets': 1,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-title');
                    }
                }, {
                    'targets': 2,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-creator');
                    }
                }, {
                    'targets': 3,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-uploader');
                    }
                }, {
                    'targets': 4,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-post_date');
                    }
                }, {
                    'targets': 5,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-menu');
                    }
                }],
                "scrollY": false,
                "processing": true,
                "ajax": {
                    url: '../content/article/data-article.php',
                    type: 'POST',
                    data: 'id=1',
                    dataType: 'json',
                },
                "columns": [{
                        "data": "no"
                    },
                    {
                        "data": "title"
                    },
                    {
                        "data": "creator"
                    },
                    {
                        "data": "uploader"
                    },
                    {
                        "data": "post_date"
                    },
                    {
                        "data": "menu"
                    }
                ]
            });
            $(document).on('click', '.btn-edit', function () {
                var id = $(this).data('table-id');
                window.location.href = "edit-article.php?id=" + id;
            })
            $(document).on('click', '.btn-delete', function () {
                var post_date = $(this).parent().parent().find("#table-post_date").html();
                var creator = $(this).parent().parent().find("#table-creator").html();
                var title = $(this).parent().parent().find("#table-title").html();
                var id = $(this).data('table-id');
                x = confirm("Apakah anda yakin menghapus data ini?");
                if (x == true) {
                    proses();
                }

                function proses() {
                    $.ajax({
                        url: '../content/article/delete-article.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'title': title,
                            'creator': creator,
                            'post_date': post_date,
                            'id': id,
                        },
                        beforeSend: function () {
                            $("input").attr('disabled', 'disabled');
                            $("textarea").attr('disabled', 'disabled');
                            $("button").attr('disabled', 'disabled');
                        },
                        complete: function () {
                            $("input").removeAttr('disabled');
                            $("textarea").removeAttr('disabled');
                            $("button").removeAttr('disabled');
                        },
                        success: function (data) {
                            $("#callout-message").slideDown();
                            if (data.status == 'success') {
                                table.ajax.reload();
                                $("#callout-message").html(data.message).focus();
                            } else {
                                $("#callout-message").html(data.message).focus();
                            }
                            timeout();
                        },
                        error: errorHandler
                    })
                }
            })
        });
    </script>
</body>

</html>
<?php } ?>
