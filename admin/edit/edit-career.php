<?php
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    header('location:../login.php');
    exit();
} else if(!isset($_GET['id'])) {
    header('location:career.php');
    exit();
} else {
    include('../content/koneksi.php');
$id = check($_GET['id']);
$sql = mysqli_query($connect, "SELECT * FROM career WHERE id='$id'");
$data = mysqli_fetch_assoc($sql);
if(mysqli_num_rows($sql) != 1){
    header('location:career.php');
    exit();
} else {
$wil=array(
	2=>array(5,'City/Region','kab')
);
if (isset($_GET['fid']) && !empty($_GET['fid'])){
    $n=strlen($_GET['fid']);
    $fid = $_GET['fid'];
    $kode = $wil[$n][0];
	$query = mysqli_query($connect, "SELECT * FROM wilayah WHERE LEFT(kode,$n)=$fid AND CHAR_LENGTH(kode)=$kode ORDER BY nama");
	echo "<option value=''>-- Select ".$wil[$n][1]." --</option>";
	while($d = mysqli_fetch_assoc($query)) {
        $aa = $d['kode'];
        $bb = $d['nama'];
        echo "<option value='".$aa."'>".$bb."</option>";
    }
}else{
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Edit Career || Admin Edwindra</title>
        <link rel="shortcut icon" href="../../public/img/favicon.ico" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="home page edwindra.com - We committed on giving services to tax
                    payer who has growing demand of tax
                    administration, tax audit, and tax dispute
                    (tax objection and tax appeal), on
                    acquisition and merger and also tax due
                    diligence" />
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <script>
            var my_ajax = do_ajax();
            var ids;
            var wil = 'kab';

            function wilayah(id) {
                if (id.length < 5) {
                    ids = id;
                    var url = "?id=" + <?php echo $id; ?> + "&sid=" + Math.random() + "&fid=" + id;
                    my_ajax.onreadystatechange = stateChanged;
                    my_ajax.open("GET", url, true);
                    my_ajax.send(null);
                }
            }

            function do_ajax() {
                if (window.XMLHttpRequest) return new XMLHttpRequest();
                if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
                return null;
            }

            function stateChanged() {
                var n = ids.length;
                var w = wil;
                var data;
                if (my_ajax.readyState == 4) {
                    data = my_ajax.responseText;
                    document.getElementById(w).innerHTML = data.length >= 0 ? data :
                        "<option selected> -- Select City/Region --</option>";
                    <?php foreach($wil as $k=>$w):?>
                    document.getElementById("<?php echo $w[2];?>_box").style.display = (n > <?php echo $k-1;?>) ?
                        'table-row' : 'none';
                    <?php endforeach;?>
                }
            }
        </script>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <style>
            #kab_box,
            #kec_box {
                display: none;
            }
        </style>
        <div class="wrapper">
            <header class="main-header">
                <a href="../index" class="logo">
                    <span class="logo-mini">
                        <b>E</b>
                    </span>
                    <span class="logo-lg">
                        <b>Edwindra</b>.com
                    </span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header text-center">ADMIN EDWINDRA</li>
                        <li>
                            <a href="../index">
                                <i class="fa fa-home"></i>
                                <span>Home</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-plus-square"></i>
                                <span>Add Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="../add/resource">
                                        <i class="fa fa-circle-o"></i> Resource</a>
                                </li>
                                <li>
                                    <a href="../add/career">
                                        <i class="fa fa-circle-o"></i> Career</a>
                                </li>
                                <li>
                                    <a href="../add/article">
                                        <i class="fa fa-circle-o"></i> Article</a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-edit"></i>
                                <span>Edit Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="resource">
                                        <i class="fa fa-circle-o"></i> Resource</a>
                                </li>
                                <li class="active">
                                    <a href="javascript:void(0)">
                                        <i class="fa fa-circle-o"></i> Career</a>
                                </li>
                                <li>
                                    <a href="article">
                                        <i class="fa fa-circle-o"></i> Article</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </section>
            </aside>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="callout callout-info" id="callout-message" style="display:none;"></div>
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title">Edit Career</h3>
                                        <div class="pull-right">
                                            <button class="btn btn-info" id="btn-back">
                                                <i class="fa fa-arrow-left"></i> Back</button>
                                        </div>
                                    </div>
                                    <div class="box-body pad">
                                        <div class="form-group">
                                            <label for="company" class="col-sm-1 col-xs-12 control-label">Company</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="company" name="company" class="form-control" placeholder="KKP Riza Edwindra" value="<?php echo $data['company']; ?>"
                                                    autocomplete="off">
                                            </div>
                                            <label for="email" class="col-sm-1 col-xs-12 control-label">Email</label>
                                            <div class="col-sm-5">
                                                <input type="email" id="email" name="email" class="form-control" placeholder="info@example.com" value="<?php echo $data['email']; ?>"
                                                    autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="job" class="col-sm-1 col-xs-12 control-label">Job</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="job" name="job" class="form-control" placeholder="Acounting" value="<?php echo $data['job']; ?>" autocomplete="off">
                                            </div>
                                            <label for="sub_job" class="col-sm-1 col-xs-12 control-label">Sub Job</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="sub_job" name="sub_job" class="form-control" placeholder="Acounting Manager" value="<?php echo $data['sub_job']; ?>" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="career_level" class="col-sm-1 col-xs-12 control-label">Career Level</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="career_level" name="career_level" class="form-control" value="<?php echo $data['career_level']; ?>" autocomplete="off">
                                            </div>
                                            <label for="education_level" class="col-sm-1 col-xs-12 control-label">Education Level</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="education_level" name="education_level" class="form-control" value="<?php echo $data['education_level']; ?>"
                                                    autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="years_of_experience" class="col-sm-1 col-xs-12 control-label">Years of Experience</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="years_of_experience" name="years_of_experience" class="form-control" value="<?php echo $data['year_experience']; ?>"
                                                    autocomplete="off">
                                            </div>
                                            <label for="minimal_education_description" class="col-sm-1 col-xs-12 control-label">Minimal Education</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="minimal_education_description" name="minimal_education_description" class="form-control" value="<?php echo $data['minimal_education_description']; ?>"
                                                    autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="job_function" class="col-sm-1 col-xs-12 control-label">Job Function</label>
                                            <div class="col-sm-5">
                                                <textarea type="text" style="resize:vertical;" id="job_function" name="job_function" class="form-control" autocomplete="off"><?php echo $data['job_function']; ?></textarea>
                                            </div>
                                            <label for="experience" class="col-sm-1 col-xs-12 control-label">Minimal Experience</label>
                                            <div class="col-sm-5">
                                                <textarea type="text" style="resize:vertical;" id="experience" name="experience" class="form-control" autocomplete="off"><?php echo strip_tags($data['experience']); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="title" class="col-sm-1 col-xs-12 control-label">Employment Term</label>
                                            <div class="col-sm-5">
                                                <select id="employment_term" name="employment_term" class="form-control">
                                                    <option value="">-- Select --</option>
                                                    <option value="1">Full Time</option>
                                                    <option value="2">Part Time</option>
                                                </select>
                                            </div>
                                            <label for="title" class="col-sm-1 col-xs-12 control-label">Expired Date</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="datepicker" name="expired_date" class="form-control expired_date" autocomplete="off" value="<?php echo $data['expired_post']; ?>"
                                                    placeholder="MM/DD/YY">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="title" class="col-sm-1 col-xs-12 control-label">Work Location</label>
                                            <div class="col-sm-11">
                                                <textarea type="text" style="resize:vertical;" id="work_location" name="work_location" class="form-control" placeholder="Address"
                                                    autocomplete="off"><?php echo explode(",", $data['work_location'])[0]; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6 col-md-offset-1 col-xs-12">
                                                <select id="prov" name="prov" class="form-control" onchange="wilayah(this.value)" style="margin-bottom:8px;">
                                                    <option value="">-- Select Province --</option>
                                                    <?php
                                                $query2=mysqli_query($connect, "SELECT kode,nama FROM wilayah WHERE CHAR_LENGTH(kode)=2 ORDER BY nama");
                                                while ($data2=mysqli_fetch_assoc($query2)) { ?>
                                                        <option value="<?php echo $data2['kode']; ?>">
                                                            <?php echo $data2['nama']; ?>
                                                        </option>
                                                        <?php } ?>
                                                        <select>
                                            </div>
                                            <?php foreach($wil as $w) {?>

                                            <div class="col-sm-5 col-xs-12" id='<?php echo $w[2];?>_box'>
                                                <select id="<?php echo $w[2];?>" name="<?php echo $w[2]; ?>" class="form-control" onchange="wilayah(this.value)" style="margin-bottom:8px;">
                                                    <option value="">-- Select
                                                        <?php echo $w[1];?> --</option>
                                                </select>
                                            </div>
                                            <?php } ?>

                                        </div>
                                        <div class="form-group">
                                            <label for="title" class="col-sm-1 col-xs-12 control-label">Requirements</label>
                                            <div class="col-sm-11">
                                                <textarea class="form-control" id="requirements" name="requirements" placeholder="Place some text here" style="width: 100%; height: 200px;resize:none;"><?php echo strip_tags($data['requirements']); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="title" class="col-sm-1 col-xs-12 control-label">Job Description</label>
                                            <div class="col-sm-11 col-xs-12">
                                                <textarea class="form-control" id="job_description" name="job_description" placeholder="Place some text here" style="width: 100%; height: 200px;resize:none;"><?php echo strip_tags($data['job_description']); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="pull-left">
                                                <button class="btn btn-primary" id="btn-submit" data-table-id="<?php echo $data['id']; ?>">Update Data</button>
                                                <button class="btn btn-primary" id="btn-submit-view" data-table-id="<?php echo $data['id']; ?>">Update & View Data</button>
                                            </div>
                                            <div class="pull-right">
                                                <button class="btn btn-danger" id="btn-reset">Reset Form</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2014-2016
                    <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights reserved.
            </footer>
        </div>

        <script src="../../public/js/jquery-3.2.1.min.js"></script>
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
        <script src="../bower_components/ckeditor/ckeditor.js"></script>
        <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="../bower_components/fastclick/lib/fastclick.js"></script>
        <script src="../dist/js/adminlte.min.js"></script>
        <script src="../dist/js/demo.js"></script>
        <script>
            $(document).ready(function () {
                $('#datepicker').datepicker({
                    autoclose: true
                });
                CKEDITOR.replace('job_description');
                $("#btn-reset").click(function () {
                    $("input").val("");
                    $("textarea").val("");
                    $("select").val("");
                });
                $("#btn-back").click(function () {
                    window.location.href = 'career.php';
                });
                $("#btn-submit").click(function () {
                    proses("", $(this).data('table-id'));
                });

                $("#btn-submit-view").click(function () {
                    proses("view", $(this).data('table-id'));
                });

                function proses(check, id) {
                    var company = $("#company").val();
                    var email = $("#email").val();
                    var job = $("#job").val();
                    var sub_job = $("#sub_job").val();
                    var career_level = $("#career_level").val();
                    var education_level = $("#education_level").val();
                    var years_of_experience = $("#years_of_experience").val();
                    var minimal_education_description = $("#minimal_education_description").val();
                    var job_function = $("#job_function").val();
                    var experience = $("#experience").val();
                    var employment_term = $("#employment_term").val();
                    var expired_date = $(".expired_date").val();
                    var work_location = $("#work_location").val();
                    var prov = $("#prov").val();
                    var kab = $("#kab").val();
                    var requirements = $("#requirements").val();
                    var job_description = CKEDITOR.instances.job_description.getData();
                    $.ajax({
                        url: '../content/career/edit-career.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'id': id,
                            'company': company,
                            'email': email,
                            'job': job,
                            'sub_job': sub_job,
                            'career_level': career_level,
                            'education_level': education_level,
                            'years_of_experience': years_of_experience,
                            'minimal_education_description': minimal_education_description,
                            'job_function': job_function,
                            'experience': experience,
                            'employment_term': employment_term,
                            'expired_date': expired_date,
                            'work_location': work_location,
                            'prov': prov,
                            'kab': kab,
                            'requirements': requirements,
                            'job_description': job_description
                        },
                        beforeSend: function () {
                            $("input").attr('disabled', 'disabled');
                            $("textarea").attr('disabled', 'disabled');
                            $("button").attr('disabled', 'disabled');
                        },
                        complete: function () {
                            $("input").removeAttr('disabled');
                            $("textarea").removeAttr('disabled');
                            $("button").removeAttr('disabled');
                        },
                        success: function (data) {
                            $("#callout-message").slideDown();
                            if (data.status == 'success') {
                                $("#callout-message").html(data.message).focus();
                                if (check == "view") {
                                    setTimeout(function () {
                                        window.location.href = window.location.origin + "/career"
                                    }, 2500);
                                }
                            } else {
                                $("#callout-message").html(data.message).focus();
                            }
                            timeout();
                        },
                        error: errorHandler
                    })
                }

                function timeout() {
                    setTimeout(function () {
                        $("#callout-message").slideUp();
                    }, 5000);
                }

                function errorHandler(jqXHR, exception) {
                    $("#callout-message").slideDown();
                    if (jqXHR.status === 0) {
                        $("#callout-message").html('Not connect.\n Verify Network.').focus();
                    } else if (jqXHR.status == 404) {
                        $("#callout-message").html('Requested page not found. [404]').focus();
                    } else if (jqXHR.status == 500) {
                        $("#callout-message").html('Internal Server Error [500].').focus();
                    } else if (exception === 'parsererror') {
                        $("#callout-message").html('Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        $("#callout-message").html('Time out error.').focus();
                    } else if (exception === 'abort') {
                        $("#callout-message").html('Ajax request aborted.').focus();
                    } else {
                        $("#callout-message").html('Uncaught Error.\n' + jqXHR.responseText).focus();
                    }
                    timeout();
                }
            });
        </script>
    </body>

    </html>
    <?php } } }?>
