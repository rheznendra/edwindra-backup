<?php
session_start();
include('../content/koneksi.php');
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    header('location:../login.php');
} else if(!isset($_GET['id'])) {
    header('location:resource.php');
} else {
$id = check($_GET['id']);
$sql = mysqli_query($connect, "SELECT * FROM resource WHERE id='$id'");
$data = mysqli_fetch_assoc($sql);
if(mysqli_num_rows($sql) != 1){
    header('location:resource.php');
} else {
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Edit Resource || Admin Edwindra</title>
        <link rel="shortcut icon" href="../../public/img/favicon.ico" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="home page edwindra.com - We committed on giving services to tax
                    payer who has growing demand of tax
                    administration, tax audit, and tax dispute
                    (tax objection and tax appeal), on
                    acquisition and merger and also tax due
                    diligence" />
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <a href="index" class="logo">
                    <span class="logo-mini">
                        <b>E</b>
                    </span>
                    <span class="logo-lg">
                        <b>Edwindra</b>.com
                    </span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header text-center">ADMIN EDWINDRA</li>
                        <li>
                            <a href="../index">
                                <i class="fa fa-home"></i>
                                <span>Home</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-plus-square"></i>
                                <span>Add Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="../add/resource">
                                        <i class="fa fa-circle-o"></i> Resource</a>
                                </li>
                                <li>
                                    <a href="../add/career">
                                        <i class="fa fa-circle-o"></i> Career</a>
                                </li>
                                <li>
                                    <a href="../add/article">
                                        <i class="fa fa-circle-o"></i> Article</a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-edit"></i>
                                <span>Edit Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="javascript:void(0)">
                                        <i class="fa fa-circle-o"></i> Resource</a>
                                </li>
                                <li>
                                    <a href="career">
                                        <i class="fa fa-circle-o"></i> Career</a>
                                </li>
                                <li>
                                    <a href="article">
                                        <i class="fa fa-circle-o"></i> Article</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </section>
            </aside>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="callout callout-info" id="callout-message" style="display:none;"></div>
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Edit Resource</h3>
                                    <div class="pull-right">
                                        <button class="btn btn-info" id="btn-back">
                                            <i class="fa fa-arrow-left"></i> Back</button>
                                    </div>
                                </div>
                                <div class="box-body pad">
                                    <div class="form-group">
                                        <label for="title">Title :</label>
                                        <input type="text" id="title" class="form-control" autocomplete="off" value="<?php echo $data['title']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Link :</label>
                                        <input type="url" id="link" class="form-control" placeholder="http://" autocomplete="off" value="<?php echo $data['link']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Download Link :</label>
                                        <input type="url" id="download_link" class="form-control" placeholder="http://" autocomplete="off" value="<?php echo $data['download_link']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Description :</label>
                                        <textarea class="form-control" id="description" placeholder="Place some text here" style="width: 100%; height: 200px;resize:none;"><?php echo $data['description']; ?></textarea>
                                    </div>
                                    <div class="box-footer">
                                        <div class="pull-left">
                                            <button class="btn btn-primary" id="btn-submit" data-table-id="<?php echo $data['id']; ?>">Update Data</button>
                                            <button class="btn btn-primary" id="btn-submit-view" data-table-id="<?php echo $data['id']; ?>">Update & View Data</button>
                                        </div>
                                        <div class="pull-right">
                                            <button class="btn btn-danger" id="btn-reset">Reset Form</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2014-2016
                    <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights reserved.
            </footer>
        </div>

        <script src="../../public/js/jquery-3.2.1.min.js"></script>
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
        <script src="../bower_components/fastclick/lib/fastclick.js"></script>
        <script src="../dist/js/adminlte.min.js"></script>
        <script src="../dist/js/demo.js"></script>
        <script>
            $(document).ready(function () {
                $("#btn-reset").click(function () {
                    $("input[id=title]").val("");
                    $("input[id=link]").val("");
                    $("textarea[id=description]").val("");
                });
                $("#btn-back").click(function () {
                    window.location.href = 'resource.php';
                });
                $("#btn-submit").click(function () {
                    proses("", $(this).data('table-id'));
                });

                $("#btn-submit-view").click(function () {
                    proses("view", $(this).data('table-id'));
                });

                function proses(check, id) {
                    var title = $("#title").val();
                    var link = $("#link").val();
                    var download_link = $("#download_link").val();
                    var description = $("#description").val();
                    $.ajax({
                        url: '../content/resource/edit-resource.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'title': title,
                            'link': link,
                            'id': id,
                            'download_link': download_link,
                            'description': description
                        },
                        beforeSend: function () {
                            $("input").attr('disabled', 'disabled');
                            $("textarea").attr('disabled', 'disabled');
                            $("button").attr('disabled', 'disabled');
                        },
                        complete: function () {
                            $("input").removeAttr('disabled');
                            $("textarea").removeAttr('disabled');
                            $("button").removeAttr('disabled');
                        },
                        success: function (data) {
                            $("#callout-message").slideDown();
                            if (data.status == 'success') {
                                $("#callout-message").html(data.message).focus();
                                if (check == "view") {
                                    setTimeout(function () {
                                        window.location.href = window.location.origin + "/resource"
                                    }, 2500);
                                }
                            } else {
                                $("#callout-message").html(data.message).focus();
                            }
                            timeout();
                        },
                        error: errorHandler
                    })
                }

                function timeout() {
                    setTimeout(function () {
                        $("#callout-message").slideUp();
                    }, 5000);
                }

                function errorHandler(jqXHR, exception) {
                    $("#callout-message").slideDown();
                    if (jqXHR.status === 0) {
                        $("#callout-message").html('Not connect.\n Verify Network.').focus();
                    } else if (jqXHR.status == 404) {
                        $("#callout-message").html('Requested page not found. [404]').focus();
                    } else if (jqXHR.status == 500) {
                        $("#callout-message").html('Internal Server Error [500].').focus();
                    } else if (exception === 'parsererror') {
                        $("#callout-message").html('Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        $("#callout-message").html('Time out error.').focus();
                    } else if (exception === 'abort') {
                        $("#callout-message").html('Ajax request aborted.').focus();
                    } else {
                        $("#callout-message").html('Uncaught Error.\n' + jqXHR.responseText).focus();
                    }
                    timeout();
                }
            });
        </script>
    </body>

    </html>
    <?php } } ?>
