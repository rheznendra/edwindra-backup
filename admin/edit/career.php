<?php
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    header('location:../login.php');
  exit();
} else {
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Career Resource || Admin Edwindra</title>
    <link rel="shortcut icon" href="../../public/img/favicon.ico" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="home page edwindra.com - We committed on giving services to tax
                    payer who has growing demand of tax
                    administration, tax audit, and tax dispute
                    (tax objection and tax appeal), on
                    acquisition and merger and also tax due
                    diligence" />
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <style>
        th {
            text-align: center;
        }

        #table-menu {
            text-align: right;
            width: 20%;
        }

        #table-id {
            text-align: center;
            width: 5%;
        }
    </style>
    <div class="wrapper">

        <header class="main-header">
            <a href="index2.html" class="logo">
                <span class="logo-mini">
                    <b>E</b>
                </span>
                <span class="logo-lg">
                    <b>Edwindra</b>.com
                </span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </nav>
        </header>

        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header text-center">ADMIN EDWINDRA</li>
                    <li>
                        <a href="../index">
                            <i class="fa fa-home"></i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-plus-square"></i>
                            <span>Add Data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="../add/resource">
                                    <i class="fa fa-circle-o"></i> Resource</a>
                            </li>
                            <li>
                                <a href="../add/career">
                                    <i class="fa fa-circle-o"></i> Career</a>
                            </li>
                            <li>
                                <a href="../add/article">
                                    <i class="fa fa-circle-o"></i> Article</a>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview active">
                        <a href="#">
                            <i class="fa fa-edit"></i>
                            <span>Edit Data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="resource">
                                    <i class="fa fa-circle-o"></i> Resource</a>
                            </li>
                            <li class="active">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-circle-o"></i> Career</a>
                            </li>
                            <li>
                                <a href="article">
                                    <i class="fa fa-circle-o"></i> Article</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </section>
        </aside>
        <div class="content-wrapper">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li>
                        <a href="#">
                            <i class="fa fa-home"></i> Home</a>
                    </li>
                    <li>
                        <a href="#">Edit Data</a>
                    </li>
                    <li class="active">Career</li>
                </ol>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="callout callout-info" id="callout-message" style="display:none;"></div>
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Data Career</h3>
                            </div>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>COMPANY</th>
                                            <th>EMAIL</th>
                                            <th>JOB</th>
                                            <th>SUB JOB</th>
                                            <th>MENU</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <footer class="main-footer ">
            <div class="pull-right hidden-xs ">
                <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy; 2014-2016
                <a href="https://adminlte.io ">Almsaeed Studio</a>.</strong> All rights reserved.
        </footer>
    </div>
    <script src="../../public/js/jquery-3.2.1.min.js "></script>
    <script src="../../bootstrap/js/bootstrap.min.js "></script>
    <script src="../bower_components/datatables.net/js/jquery.dataTables.min.js "></script>
    <script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js "></script>
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js "></script>
    <script src="../bower_components/fastclick/lib/fastclick.js "></script>
    <script src="../dist/js/adminlte.min.js "></script>
    <script src="../dist/js/demo.js "></script>
    <script>
        $(document).ready(function () {
            var table = $('#example1').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-id');
                    }
                }, {
                    'targets': 1,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-company');
                    }
                }, {
                    'targets': 2,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-email');
                    }
                }, {
                    'targets': 3,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-job');
                    }
                }, {
                    'targets': 4,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-sub_job');
                    }
                }, {
                    'targets': 5,
                    'createdCell': function (td) {
                        $(td).attr('id', 'table-menu');
                    }
                }],
                "scrollY": false,
                "processing": true,
                "ajax": {
                    url: '../content/career/data-career.php',
                    type: 'POST',
                    data: 'id=1',
                    dataType: 'json',
                },
                "columns": [{
                        "data": "no"
                    },
                    {
                        "data": "company"
                    },
                    {
                        "data": "email"
                    },
                    {
                        "data": "job"
                    },
                    {
                        "data": "sub_job"
                    },
                    {
                        "data": "menu"
                    }
                ]
            });
            $(document).on('click', '.btn-edit', function () {
                var id = $(this).data('table-id');
                window.location.href = "edit-career.php?id=" + id;
            })
            $(document).on('click', '.btn-delete', function () {
                var company = $(this).parent().parent().find("#table-company").html();
                var email = $(this).parent().parent().find("#table-email").html();
                var sub_job = $(this).parent().parent().find("#table-sub_job").html();
                var job = $(this).parent().parent().find("#table-job").html();
                var id = $(this).data('table-id');
                x = confirm("Apakah anda yakin menghapus data ini?");
                if (x == true) {
                    proses();
                }

                function proses() {
                    $.ajax({
                        url: '../content/career/delete-career.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'company': company,
                            'email': email,
                            'sub_job': sub_job,
                            'job': job,
                            'id': id,
                        },
                        beforeSend: function () {
                            $("input").attr('disabled', 'disabled');
                            $("textarea").attr('disabled', 'disabled');
                            $("button").attr('disabled', 'disabled');
                        },
                        complete: function () {
                            $("input").removeAttr('disabled');
                            $("textarea").removeAttr('disabled');
                            $("button").removeAttr('disabled');
                        },
                        success: function (data) {
                            $("#callout-message").slideDown();
                            if (data.status == 'success') {
                                table.ajax.reload();
                                $("#callout-message").html(data.message).focus();
                            } else {
                                $("#callout-message").html(data.message).focus();
                            }
                            timeout();
                        },
                        error: errorHandler
                    })
                }

                function timeout() {
                    setTimeout(function () {
                        $("#callout-message").slideUp();
                    }, 5000);
                }

                function errorHandler(jqXHR, exception) {
                    $("#callout-message").slideDown();
                    if (jqXHR.status === 0) {
                        $("#callout-message").html('Not connect.\n Verify Network.').focus();
                    } else if (jqXHR.status == 404) {
                        $("#callout-message").html('Requested page not found. [404]').focus();
                    } else if (jqXHR.status == 500) {
                        $("#callout-message").html('Internal Server Error [500].').focus();
                    } else if (exception === 'parsererror') {
                        $("#callout-message").html('Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        $("#callout-message").html('Time out error.').focus();
                    } else if (exception === 'abort') {
                        $("#callout-message").html('Ajax request aborted.').focus();
                    } else {
                        $("#callout-message").html('Uncaught Error.\n' + jqXHR.responseText).focus();
                    }
                    timeout();
                }
            })
        });
    </script>
</body>

</html>
<?php } ?>
