function timeout() {
    setTimeout(function() {
        $("#callout-message").slideUp();
    }, 5000);
}

function errorHandler(jqXHR, exception) {
    $("#callout-message").slideDown();
    if (jqXHR.status === 0) {
        $("#callout-message").html('Not connect.\n Verify Network.').focus();
    } else if (jqXHR.status == 404) {
        $("#callout-message").html('Requested page not found. [404]').focus();
    } else if (jqXHR.status == 500) {
        $("#callout-message").html('Internal Server Error [500].').focus();
    } else if (exception === 'parsererror') {
        $("#callout-message").html('Requested JSON parse failed.');
    } else if (exception === 'timeout') {
        $("#callout-message").html('Time out error.').focus();
    } else if (exception === 'abort') {
        $("#callout-message").html('Ajax request aborted.').focus();
    } else {
        $("#callout-message").html('Uncaught Error.\n' + jqXHR.responseText).focus();
    }
    timeout();
}