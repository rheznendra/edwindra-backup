<?php
session_start();
if(!isset($_SESSION['login'])) {
    header('location:login.php');
    exit();
} else { ?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Admin Edwindra</title>
        <?php include('item/css.php'); ?>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <a href="index" class="logo">
                    <span class="logo-mini">
                        <b>E</b>
                    </span>
                    <span class="logo-lg">
                        <b>Edwindra</b>.com
                    </span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                </nav>
            </header>

            <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header text-center">ADMIN EDWINDRA</li>
                        <li class="active">
                            <a href="javascript:void(0)">
                                <i class="fa fa-home"></i>
                                <span>Home</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-plus-square"></i>
                                <span>Add Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="add/resource">
                                        <i class="fa fa-circle-o"></i> Resource</a>
                                </li>
                                <li>
                                    <a href="add/career">
                                        <i class="fa fa-circle-o"></i> Career</a>
                                </li>
                                <li>
                                    <a href="add/article">
                                        <i class="fa fa-circle-o"></i> Article</a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i>
                                <span>Edit Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="edit/resource">
                                        <i class="fa fa-circle-o"></i> Resource</a>
                                </li>
                                <li>
                                    <a href="edit/career">
                                        <i class="fa fa-circle-o"></i> Career</a>
                                </li>
                                <li>
                                    <a href="edit/article">
                                        <i class="fa fa-circle-o"></i> Article</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="logout">
                                <i class="fa fa-power-off"></i>
                                <span>Sign Out</span>
                            </a>
                        </li>
                    </ul>
                </section>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <ol class="breadcrumb">
                        <li>
                            <a href="#">
                                <i class="fa fa-home"></i> Home</a>
                        </li>
                    </ol>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="box box-primary">
                                <div class="box-body">
                            <center><h1><b>WELCOME TO ADMIN EDWINDRA <img src="../public/img/LOGO KKP fixed.png" width="100" height="80"></b></h1></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include('item/footer.php'); ?>

        </div>
        <?php include('item/js.php'); ?>
    </body>

    </html>
    <?php
}
?>