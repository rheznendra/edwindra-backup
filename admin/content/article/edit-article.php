<?php
header('Content-Type: application/json');
date_default_timezone_set("Asia/Makassar");
include('../koneksi.php');
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
  exit();
} else {
if (!isset($_POST['title'],$_POST['editor'],$_POST['id'])) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
    exit();
} else if(empty($_POST['title']) || empty($_POST['editor']) || empty($_POST['id'])) {
    $result = array('status' => 'error', 'message' => 'Form tidak boleh kosong!');
    echo json_encode($result);
    exit();
} else {
    $editor = check($_POST['editor']);
    $title = check($_POST['title']);
    $id = check($_POST['id']);
    $date = date("m/d/Y h:i");
    $sql = mysqli_query($connect, "SELECT * FROM article WHERE id='$id'");
    $data = mysqli_fetch_assoc($sql);
    if(empty($_POST['title'])) {
        $title = $data['title'];
    }
    if(empty($data['editor']) && empty($_POST['editor'])) {
        $result = array('status' => 'error', 'message' => 'Form editor tidak boleh kosong!');
        echo json_encode($result);
        exit();
    } else if(!empty($data['editor']) && empty($_POST['editor'])) {
        $editor = $data['editor'];
    }
if(isset($_POST['description'])) {
    if(empty($_POST['description'])) {
    $description =  $data['description'];
    } else {
    $description = check($_POST['description']);
    }
    $query = mysqli_query($connect, "UPDATE article SET title='$title',edit_date='$date', editor='$editor', description='$description' WHERE id='$id'");
} else if(isset($_POST['short_description'])) {
    if(empty($_POST['short_description'])) {
        $short_description = $data['short_desc'];
    } else {
        $short_description = check($_POST['short_description']);
    }
    if(empty($_FILES['pdf_file']['name']) || $_FILES['pdf_file']['error'] == 1) {
        $query = mysqli_query($connect, "UPDATE article SET title='$title',edit_date='$date', editor='$editor',short_desc='$short_description' WHERE id='$id'");
    } else {
        $file_name = $_FILES['pdf_file']['name'];
        $temp = explode(".", $file_name);
        $newfilename = round(microtime(true)) . '.' . end($temp);
        $dir = '../../../pdf/';
        $target = $dir.basename($newfilename);
        $filetype = strtolower(pathinfo($target,PATHINFO_EXTENSION));
        if($filetype != 'pdf') {
            $result = array('status' => 'error', 'message' => 'Format file tidak valid!');
            echo json_encode($result);
            exit();
        }
        if (move_uploaded_file($_FILES['pdf_file']['tmp_name'], $dir.$newfilename)) {
            $query = mysqli_query($connect, "UPDATE article SET title='$title',edit_date='$date', editor='$editor',short_desc='$short_description', file_name='$newfilename' WHERE id='$id'");
            unlink("../../../pdf/".$data['file_name']);
        } else {
            $result = array('status' => 'error', 'message' => "File gagal diupload!");
            echo json_encode($result);
            exit();
        }
    }
}
    if($query) {
        $result = json_encode(array('status' => 'success', 'message' => "Data berhasil diupdate!"));
    } else {
        $result = json_encode(array('status' => 'error', 'message' => "Data gagal diupdate!"));
    }
    echo $result;
}
}
?>
