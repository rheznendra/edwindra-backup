<?php
session_start();
header('Content-Type: application/json');
date_default_timezone_set("Asia/Makassar");
include('../koneksi.php');
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan! 1');
    echo json_encode($result);
  exit();
} else {
if (!isset($_POST['title'],$_POST['creator'],$_POST['uploader'])) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!1');
    echo json_encode($result);
    exit();
} else if(empty($_POST['title']) || empty($_POST['creator']) || empty($_POST['uploader'])) {
    $result = array('status' => 'error', 'message' => 'Form tidak boleh kosong!');
    echo json_encode($result);
    exit();
} else {
    $creator = check($_POST['creator']);
    $title = check($_POST['title']);
    $post_date = date("M d Y");
    $uploader = check($_POST['uploader']);
if(isset($_POST['description'])) {
    if(empty($_POST['description'])) {
        $result = array('status' => 'error', 'message' => 'Form tidak boleh kosong!');
        echo json_encode($result);
        exit();
    } else {
    $description =  check($_POST['description']);
    $query = mysqli_query($connect, "INSERT INTO article VALUES('','$title','$post_date','$creator','$uploader','','$description','','','0','')");
    }
} else if(isset($_FILES['pdf_file'],$_POST['short_description'])) {
    if(empty($_POST['short_description']) || empty($_FILES['pdf_file'])) {
        $result = array('status' => 'error', 'message' => $_POST['short_description']);
        echo json_encode($result);
        exit();
    } else {
    $short_description = check($_POST['short_description']);
    $file_name = $_FILES['pdf_file']['name'];
    $temp = explode(".", $file_name);
    $newfilename = round(microtime(true)) . '.' . end($temp);
    $dir = '../../../pdf/';
    $target = $dir.basename($newfilename);
    $filetype = strtolower(pathinfo($target,PATHINFO_EXTENSION));
    if($filetype != 'pdf') {
        $result = array('status' => 'error', 'message' => 'Format file tidak valid!');
        echo json_encode($result);
        exit();
    }
    if (move_uploaded_file($_FILES['pdf_file']['tmp_name'], $dir.$newfilename)) {
    $query = mysqli_query($connect, "INSERT INTO article VALUES('','$title','$post_date','$creator','$uploader','','','$newfilename','$short_description','1','')");
    } else {
        $result = array('status' => 'error', 'message' => $file_name);
        echo json_encode($result);
        exit();
    }
    }
}
    if($query) {
        $result = json_encode(array('status' => 'success', 'message' => "Data berhasil ditambahkan!"));
    } else {
        $result = json_encode(array('status' => 'error', 'message' => "Data gagal ditambahkan!"));
    }
    echo $result;
}
}
?>
