<?php
class proses
{
    private $hasil;
    public $username, $password;

    public function __construct()
    {
        
        session_start();
        header('Content-Type: application/json');
    }
    public function logout()
    {
        session_destroy();
        header("location:login.php");
    }
    public function login()
    {
        if ($this->username == "admin" && $this->password == "admin") {
            $result = array('status' => 'success');
            $this->hasil = json_encode($result);
            return $this->hasil;
        } else {
            $result = array('status' => 'error', 'message' => 'Username atau Password yang anda masukkan belum terdaftar!');
            $this->hasil = json_encode($result);
            return $this->hasil;
        }
    }
}
