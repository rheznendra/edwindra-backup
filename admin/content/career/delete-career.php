<?php
header('Content-Type: application/json');
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
  exit();
} else {
if (!isset($_POST['company'],$_POST['email'],$_POST['job'],$_POST['sub_job'],$_POST['id'])) {
    $result = array('status' => 'error', 'message' => 'Terjadi kesalahan!1');
} else if(empty($_POST['company']) || empty($_POST['email']) || empty($_POST['job']) || empty($_POST['sub_job']) || empty($_POST['id'])) {
    $result = array('status' => 'error', 'message' => 'Terjadi kesalahan!2');
} else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
$result = json_encode(array('status' => 'error', 'message' => "Terjadi kesalahan!3"));
} else {
    include('../koneksi.php');
    $company = check($_POST['company']);
    $id = check($_POST['id']);
    $job = check($_POST['job']);
    $sub_job = check($_POST['sub_job']);
    $email = check($_POST['email']);
    $query = mysqli_query($connect, "DELETE FROM career WHERE id='$id' AND company='$company' AND email='$email' AND job='$job' AND sub_job='$sub_job'");
    if($query) {
        $result = array('status' => 'success', 'message' => "Data berhasil dihapus!");
    } else {
        $result = array('status' => 'error', 'message' => "Data gagal dihapus!");
    }
}
echo json_encode($result);
}
?>
