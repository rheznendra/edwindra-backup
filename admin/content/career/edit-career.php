<?php
header('Content-Type: application/json');
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
  exit();
} else {
if (!isset($_POST['id'],
    $_POST['company'],
    $_POST['email'],
    $_POST['job'],
    $_POST['sub_job'],
    $_POST['career_level'],
    $_POST['education_level'],
    $_POST['education_level'],
    $_POST['years_of_experience'],
    $_POST['minimal_education_description'],
    $_POST['job_function'],
    $_POST['experience'],
    $_POST['employment_term'],
    $_POST['expired_date'],
    $_POST['work_location'],
    $_POST['prov'],
    $_POST['kab'],
    $_POST['requirements'],
    $_POST['job_description'])) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan! 1');
    echo json_encode($result);
    exit();
} else if(
    empty($_POST['id']) &&
    empty($_POST['company']) &&
    empty($_POST['email']) &&
    empty($_POST['job']) &&
    empty($_POST['sub_job']) &&
    empty($_POST['career_level']) &&
    empty($_POST['education_level']) &&
    empty($_POST['years_of_experience']) &&
    empty($_POST['minimal_education_description']) &&
    empty($_POST['job_function']) &&
    empty($_POST['experience']) &&
    empty($_POST['employment_term']) &&
    empty($_POST['expired_date']) &&
    empty($_POST['work_location']) &&
    empty($_POST['prov']) &&
    empty($_POST['requirements']) &&
    empty($_POST['job_description'])) {
    $result = array('status' => 'error', 'message' => 'Form tidak boleh kosong!');
    echo json_encode($result);
    exit();
} else {
    include('../koneksi.php');
    $id = check($_POST['id']);
    $company = check($_POST['company']);
    $email = check($_POST['email']);
    $job = check($_POST['job']);
    $sub_job = check($_POST['sub_job']);
    $career_level = check($_POST['career_level']);
    $education_level = check($_POST['education_level']);
    $years_of_experience = check($_POST['years_of_experience']);
    $minimal_education_description = check($_POST['minimal_education_description']);
    $job_function = check($_POST['job_function']);
    $experience = check($_POST['experience']);
    $employment_term = check($_POST['employment_term']);
    $expired_date = check($_POST['expired_date']);
    $work_location = check($_POST['work_location']);
    $prov = check($_POST['prov']);
    $kab = check($_POST['kab']);
    $requirements = check($_POST['requirements']);
    $job_description = check($_POST['job_description']);

    $sql = mysqli_query($connect, "SELECT * FROM career WHERE id='$id'");
    $data = mysqli_fetch_assoc($sql);
    //validasi wilayah
    if(empty($_POST['prov']) && empty($_POST['kab'])) {
        $prov = $data['prov'] ;
        $kab = $data['kab'];
    } else {
    if(!empty($_POST['kab'])) {
    $array = array($prov, $kab);
    } else {
        $array = array($prov);
    }
    foreach($array as $arr) {
    $query2 = mysqli_query($connect, "SELECT * FROM wilayah WHERE kode = '$arr'");
    if(mysqli_num_rows($query2) == 0) {
        $result = json_encode(array('status' => 'error', 'message' => "Terjadi kesalahan!"));
        echo $result;
        exit();
    } else {
    $data2 = mysqli_fetch_assoc($query2);
    $array2[] = $data2['nama'];
    }
    }
    $prov = $array2[0];
    }
    //validasi email
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $result = json_encode(array('status' => 'error', 'message' => "Format email tidak valid!"));
        echo $result;
        exit();
    }
    //validasi option
    if(!empty($_POST['employment_term'])) {

    if($employment_term == 1) {
        $employment_term = "Full Time";
    } else if($employment_term == 2) {
        $employment_term = "Part Time";
    } else {
        $result = json_encode(array('status' => 'error', 'message' => "Terjadi kesalahan! 3"));
        echo $result;
        exit();
    }
} else {
    $employment_term = $data['employment_term'];
}
    //tambah li
    if(!empty($experience)) {
    $experience = "<li>". str_replace('\n',"</li>
<li>", $experience) . "</li>";
    $requirements = '<li class="list-item">'. str_replace('\n','</li>
<li class="list-item">', $requirements) . "</li>";
    }
    //validasi tgl
    if(!empty($expired_date)) {
    $date = explode("/", $expired_date);
    if(!checkdate($date[0], $date[1], $date[2])) {
        $result = json_encode(array('status' => 'error', 'message' => "Tanggal tidak valid!"));
        echo $result;
        exit();
    }
}
    if(empty($_POST['company'])) {
        $company = $data['company'];
    } else if(empty($_POST['email'])) {
        $email = $data['email'];
    } else if(empty($_POST['job'])) {
        $job = $data['job'];
    } else if(empty($_POST['sub_job'])) {
        $sub_job = $data['sub_job'];
    } else if(empty($_POST['career_level'])) {
        $career_level = $data['career_level'];
    } else if(empty($_POST['education_level'])) {
        $education_level = $data['education_level'];
    } else if(empty($_POST['years_of_experience'])) {
        $years_of_experience = $data['years_of_experience'];
    } else if(empty($_POST['minimal_education_description'])) {
        $minimal_education_description = $data['minimal_education_description'];
    } else if(empty($_POST['job_function'])) {
        $job_function = $data['job_function'];
    } else if(empty($_POST['experience'])) {
        $experience = $data['experience'];
    } else if(empty($_POST['expired_date'])) {
        $expired_date = $data['expired_date'];
    } else if(empty($_POST['requirements'])) {
        $requirements = $data['requirements'];
    } else if(empty($_POST['job_description'])) {
        $job_description = $data['job_description'];
    } else if(empty($_POST['work_location'])) {
        $work_location = $data['work_location'];
    }
    if(!empty($_POST['kab'])) {
    $kab = $array2[1];
    $query = mysqli_query($connect, "UPDATE career SET company='$company',email='$email',job='$job',sub_job='$sub_job',career_level='$career_level',education_level='$education_level',minimal_education_description='$minimal_education_description',job_function='$job_function',experience='$experience',year_experience='$years_of_experience',employment_term='$employment_term',expired_post='$expired_date',requirements='$requirements',job_description='$job_description', kab='$kab', prov='$prov' WHERE id='$id'");
    } else {
    $query = mysqli_query($connect, "UPDATE career SET company='$company',email='$email',job='$job',sub_job='$sub_job',career_level='$career_level',education_level='$education_level',minimal_education_description='$minimal_education_description',job_function='$job_function',experience='$experience',year_experience='$years_of_experience',employment_term='$employment_term',expired_post='$expired_date',requirements='$requirements',job_description='$job_description', prov='$prov',kab='' WHERE id='$id'");
    }
    if($query) {
        $result = json_encode(array('status' => 'success', 'message' => "Data berhasil diedit!"));
    } else {
        $result = json_encode(array('status' => 'error', 'message' => "Data gagal diedit!"));
    }
    echo $result;
}
}
?>
