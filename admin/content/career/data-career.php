<?php
header('Content-Type: application/json');
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
  exit();
} else {
if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
    header( 'HTTP/1.0 403 Forbidden', TRUE, 403 );
    die( header( 'location: ./' ) );
} else {
include('../koneksi.php');
$sql = mysqli_query($connect, "SELECT * FROM career");
$no = 1;
if(mysqli_num_rows($sql) == 0) {
    print_r(json_encode(array(
        'status' => 'error', 'data' => '')));
} else {
while ($row = mysqli_fetch_assoc($sql))
{
    $row['no'] = $no++;
    $row['menu'] = '<td class="table-menu"><button class="btn btn-primary btn-edit" data-table-id="'.$row['id'].'">Edit</button> <button class="btn btn-danger btn-delete" data-table-id="'.$row['id'].'">Delete</button></td>';
    $results[] = $row;
}
print_r(json_encode(array(
    'status' => 'success', 'data' => $results)));}
}
}
?>
