<?php
header('Content-Type: application/json');
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
  exit();
} else {
if (!isset($_POST['title'],$_POST['link'],$_POST['id'])) {
    $result = array('status' => 'error', 'message' => 'Terjadi kesalahan!');
} else if(empty($_POST['title']) || empty($_POST['link']) || empty($_POST['id'])) {
    $result = array('status' => 'error', 'message' => 'Terjadi kesalahan!');
} else if(!filter_var($_POST['link'], FILTER_VALIDATE_URL)) {
    $result = array('status' => 'error', 'message' => 'Terjadi kesalahan!');
} else {
    include('../koneksi.php');
    $link = check($_POST['link']);
    $id = check($_POST['id']);
    $title = check($_POST['title']);
    $query = mysqli_query($connect, "DELETE FROM resource WHERE id='$id' AND title='$title' AND link='$link'");
    if($query) {
        $result = array('status' => 'success', 'message' => "Data berhasil dihapus!");
    } else {
        $result = array('status' => 'error', 'message' => "Data gagal dihapus!");
    }
}
echo json_encode($result);
}
?>
