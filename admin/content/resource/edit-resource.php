<?php
header('Content-Type: application/json');
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
  exit();
} else {
if (!isset($_POST['title'],$_POST['link'],$_POST['description'],$_POST['download_link'])) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
} else if(empty($_POST['title']) && empty($_POST['link']) && empty($_POST['description']) && empty($_POST['id']) && empty($_POST['download_link'])) {
    $result = array('status' => 'error', 'message' => 'Form tidak boleh kosong!');
    echo json_encode($result);
} else {
    include('../koneksi.php');
    $link = check($_POST['link']);
    $title = check($_POST['title']);
    $id = check($_POST['id']);
    $download_link = check($_POST['download_link']);
    $description =  check($_POST['description']);
    if(strlen($description) > 200) {
        $description = substr($description,0,200);
    } else {
        $description = substr($description,0,200);
    }
    $sql = mysqli_query($connect, "SELECT * FROM resource WHERE id='$id'");
    $data = mysqli_fetch_assoc($sql);
    if(empty($_POST['link'])) {
        $link = $data['link'];
    } else if(empty($_POST['title'])) {
        $title = $data['title'];
    } else if(empty($_POST['description'])) {
        $description = $data['description'];
    }
    if(!filter_var($link, FILTER_VALIDATE_URL)) {
        $result = array('status' => 'error', 'message' => 'Link tidak valid!');
        echo json_encode($result);
        exit();
    } else if(!filter_var($download_link, FILTER_VALIDATE_URL)) {
        $result = array('status' => 'error', 'message' => 'Link tidak valid!');
        echo json_encode($result);
        exit();
    } else {
    $query = mysqli_query($connect, "UPDATE resource SET title='$title', description='$description',link='$link', download_link='$download_link' WHERE id='$id'");
    if($query) {
        $result = json_encode(array('status' => 'success', 'message' => "Data berhasil diedit!"));
    } else {
        $result = json_encode(array('status' => 'error', 'message' => "Data gagal diedit!"));
    }
    echo $result;
} } }
?>
