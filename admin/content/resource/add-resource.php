<?php
header('Content-Type: application/json');
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
  exit();
} else {
if (!isset($_POST['title'],$_POST['link'],$_POST['description'])) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
} else if(empty($_POST['title']) || empty($_POST['link']) || empty($_POST['description'])) {
    $result = array('status' => 'error', 'message' => 'Form tidak boleh kosong!');
    echo json_encode($result);
} else if(!filter_var($_POST['link'], FILTER_VALIDATE_URL)) {
    $result = array('status' => 'error', 'message' => 'Link tidak valid!');
    echo json_encode($result);
    exit();
} else {
    include('../koneksi.php');
    $link = check($_POST['link']);
    $title = check($_POST['title']);
    $description =  check($_POST['description']);
    if(!empty($_POST['download_link'])) {
        $download_link = check($_POST['download_link']);
        if(!filter_var($_POST['download_link'], FILTER_VALIDATE_URL)) {
            $result = array('status' => 'error', 'message' => 'Link tidak valid!');
            echo json_encode($result);
            exit();
        } else {
            $query = mysqli_query($connect, "INSERT INTO resource VALUES('','$title','$description','$link','$download_link')");
        }
    } else {
        $query = mysqli_query($connect, "INSERT INTO resource VALUES('','$title','$description','$link','')");}
    if($query) {
        $result = json_encode(array('status' => 'success', 'message' => "Data berhasil ditambahkan!"));
    } else {
        $result = json_encode(array('status' => 'error', 'message' => "Data gagal ditambahkan!"));
    }
    echo $result;
}
}
?>
