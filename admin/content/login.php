<?php
if (!isset($_POST['username'],$_POST['password'])) {
    $result = array('status' => 'error', 'message' => 'Terjadi Kesalahan!');
    echo json_encode($result);
} else if(empty($_POST['username']) || empty($_POST['password'])) {
    $result = array('status' => 'error', 'message' => 'Username atau password tidak boleh kosong!');
    echo json_encode($result);
} else {
include("class.php");
$var = new proses();
$var->username = $_POST['username'];
$var->password = $_POST['password'];
$result = json_decode($var->login());
if($result->status == 'success') {
    $_SESSION['login'] = 1;
}
echo $var->login();
}
?>
