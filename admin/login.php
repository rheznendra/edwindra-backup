<?php
session_start();
if(isset($_SESSION['login'])) {
    header("location:edit/resource.php");
    exit();
} else {
    ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Edwindra || Admin Login</title>
    <link rel="shortcut icon" href="../public/img/favicon.ico" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="home page edwindra.com - We committed on giving services to tax
                    payer who has growing demand of tax
                    administration, tax audit, and tax dispute
                    (tax objection and tax appeal), on
                    acquisition and merger and also tax due
                    diligence" />
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="index.html">
                <b>Admin Login</b>
            </a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Login untuk mengakses.</p>
            <div class="callout callout-success" id="callout-message" style="display:none;"></div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" id="username" autocomplete="false" placeholder="Email">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" id="password" autocomplete="false" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <button type="submit" id="btn-submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </div>
        </div>
    </div>
    <script src="../public/js/jquery-3.2.1.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#btn-submit").click(function() {
                var username = $("#username").val();
                var password = $("#password").val();
                $.ajax({
                    url: 'content/login.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'username': username,
                        'password': password
                    },
                    beforeSend: function() {
                        $("input").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete: function() {
                        $("input").removeAttr('disabled');
                        $("button").removeAttr('disabled');
                    },
                    success: function(data) {
                        $("#callout-message").slideDown();
                        if (data.status == 'success') {
                            $("#callout-message").html("Login berhasil");
                            setTimeout(function() {
                                window.location.href = "    index.php"
                            }, 2500);
                        } else {
                            $("#callout-message").html(data.message);
                            setTimeout(function() {
                                $("#callout-message").slideUp();
                            }, 5000);
                        }
                    },
                    error: errorHandler
                })
            });

            function errorHandler(jqXHR, exception) {
                $("#callout-message").slideDown();
                if (jqXHR.status === 0) {
                    $("#callout-message").html('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    $("#callout-message").html('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    $("#callout-message").html('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    $("#callout-message").html('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    $("#callout-message").html('Time out error.');
                } else if (exception === 'abort') {
                    $("#callout-message").html('Ajax request aborted.');
                } else {
                    $("#callout-message").html('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    </script>
</body>

</html>
    <?php } ?>
