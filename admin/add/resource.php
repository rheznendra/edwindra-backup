<?php
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    header('location:../login.php');
  exit();
} else {
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Add Resource || Admin Edwindra</title>
        <link rel="shortcut icon" href="../../public/img/favicon.ico" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="home page edwindra.com - We committed on giving services to tax
                    payer who has growing demand of tax
                    administration, tax audit, and tax dispute
                    (tax objection and tax appeal), on
                    acquisition and merger and also tax due
                    diligence" />
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <a href="../index" class="logo">
                    <span class="logo-mini">
                        <b>E</b>
                    </span>
                    <span class="logo-lg">
                        <b>Edwindra</b>.com
                    </span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header text-center">ADMIN EDWINDRA</li>
                        <li>
                            <a href="../index">
                                <i class="fa fa-home"></i>
                                <span>Home</span>
                            </a>
                        </li>
                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-plus-square"></i>
                                <span>Add Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="javascript:void(0)">
                                        <i class="fa fa-circle-o"></i> Resource</a>
                                </li>
                                <li>
                                    <a href="career">
                                        <i class="fa fa-circle-o"></i> Career</a>
                                </li>
                                <li>
                                    <a href="article">
                                        <i class="fa fa-circle-o"></i> Article</a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i>
                                <span>Edit Data</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="../edit/resource">
                                        <i class="fa fa-circle-o"></i> Resource</a>
                                </li>
                                <li>
                                    <a href="../edit/career">
                                        <i class="fa fa-circle-o"></i> Career</a>
                                </li>
                                <li>
                                    <a href="../edit/article">
                                        <i class="fa fa-circle-o"></i> Article</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </section>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <ol class="breadcrumb">
                        <li>
                            <a href="#">
                                <i class="fa fa-home"></i> Home</a>
                        </li>
                        <li>
                            <a href="#">Add Data</a>
                        </li>
                        <li class="active">Resource</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="callout callout-info" id="callout-message" style="display:none;"></div>
                            <div class="box box-primary">
                                <div class="box-body pad">
                                    <div class="form-group">
                                        <label for="title">Title :</label>
                                        <input type="text" id="title" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Link :</label>
                                        <input type="url" id="link" class="form-control" placeholder="http://" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Download Link :</label>
                                        <input type="url" id="download_link" class="form-control" placeholder="http://" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Description :</label>
                                        <textarea class="form-control" id="description" placeholder="Place some text here" style="width: 100%; height: 200px;resize:none;"></textarea>
                                    </div>
                                    <div class="box-footer">
                                        <div class="pull-left form-group col-xs-7" style="padding:0;">
                                            <button class="btn btn-primary" id="btn-submit" style="margin-bottom:8px;">Add Data</button>
                                            <button class="btn btn-primary" id="btn-submit-view" style="margin-bottom:8px;">Add & View Data</button>
                                        </div>
                                        <div class="pull-right form-group col-xs-5 col-md-2">
                                            <button class="btn btn-danger" id="btn-reset" style="margin-bottom:8px;">Reset Form</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2014-2016
                    <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights reserved.
            </footer>
        </div>

        <script src="../../public/js/jquery-3.2.1.min.js"></script>
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
        <script src="../bower_components/fastclick/lib/fastclick.js"></script>
        <script src="../dist/js/adminlte.min.js"></script>
        <script src="../dist/js/demo.js"></script>
        <script>
            $(document).ready(function () {
                $("#btn-reset").click(function () {
                    $("input").val("");
                    $("textarea").val("");
                });
                $("#btn-submit").click(function () {
                    proses();
                });

                $("#btn-submit-view").click(function () {
                    proses("view");
                });


                function proses(check) {
                    var title = $("#title").val();
                    var link = $("#link").val();
                    var download_link = $("#download_link").val();
                    var description = $("#description").val();
                    if(title != "") {
                    var num = 1;
                    } else {
                    var num = 0;
                    }
                    $.ajax({
                        url: '../content/resource/add-resource.php',
                        type: 'POST',
                        dataType: 'json',
                        processData: (num == 1 ? true : false),
                        contentType: (num == 1 ? "application/x-www-form-urlencoded; charset=UTF-8" : false),
                        data: {
                            'title': title,
                            'link': link,
                            'download_link': download_link,
                            'description': description
                        },
                        beforeSend: function () {
                            $("input").attr('disabled', 'disabled');
                            $("textarea").attr('disabled', 'disabled');
                            $("button").attr('disabled', 'disabled');
                        },
                        complete: function () {
                            $("input").removeAttr('disabled');
                            $("textarea").removeAttr('disabled');
                            $("button").removeAttr('disabled');
                        },
                        success: function (data) {
                            $("#callout-message").slideDown();
                            if (data.status == 'success') {
                                $("#callout-message").html(data.message).focus();
                                if (check == "view") {
                                    setTimeout(function () {
                                        window.location.href = window.location.origin +  "/resource";
                                    }, 2500);
                                }
                            } else {
                                $("#callout-message").html(data.message).focus();
                            }
                            timeout();
                        },
                        error: errorHandler
                    })
                }

                function timeout() {
                    setTimeout(function () {
                        $("#callout-message").slideUp();
                    }, 5000);
                }

                function errorHandler(jqXHR, exception) {
                    $("#callout-message").slideDown();
                    if (jqXHR.status === 0) {
                        $("#callout-message").html('Not connect.\n Verify Network.').focus();
                    } else if (jqXHR.status == 404) {
                        $("#callout-message").html('Requested page not found. [404]').focus();
                    } else if (jqXHR.status == 500) {
                        $("#callout-message").html('Internal Server Error [500].').focus();
                    } else if (exception === 'parsererror') {
                        $("#callout-message").html('Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        $("#callout-message").html('Time out error.').focus();
                    } else if (exception === 'abort') {
                        $("#callout-message").html('Ajax request aborted.').focus();
                    } else {
                        $("#callout-message").html('Uncaught Error.\n' + jqXHR.responseText).focus();
                    }
                    timeout();
                }
            });
        </script>
    </body>

    </html>
    <?php } ?>
