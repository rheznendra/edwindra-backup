<?php
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    header('location:../login.php');
  exit();
} else {
$dbhost ='localhost';
$dbuser ='u6989902_admin';
$dbpass ='u6989902_admin';
$dbname ='u6989902_admin';
$dbdsn = "mysql:dbname=$dbname;host=$dbhost";
try {
  $db = new PDO($dbdsn, $dbuser, $dbpass);
} catch (PDOException $e) {
  echo 'Connection failed: '.$e->getMessage();
}
$wil=array(
	2=>array(5,'City/Region','kab'),
);
if (isset($_GET['id']) && !empty($_GET['id'])){
	$n=strlen($_GET['id']);
	$query = $db->prepare("SELECT * FROM wilayah WHERE LEFT(kode,:n)=:id AND CHAR_LENGTH(kode)=:m ORDER BY nama");
	$query->execute(array(':n'=>$n,':id'=>$_GET['id'],':m'=>$wil[$n][0]));
	echo"<option value=''>-- Select {$wil[$n][1]} --</option>";
	while($d = $query->fetchObject())
		echo "<option value='{$d->kode}'>{$d->nama}</option>";
}else{
?>
<!DOCTYPE html>
<html>

<head>
    <title>Admin Edwindra</title>
<?php include('../item/css.php'); ?>
    <link rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME']."/edwindra"; ?>/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <script>
    var my_ajax = do_ajax();
            var ids;
            var wil = 'kab';

            function wilayah(id) {
                if (id.length < 5) {
                    ids = id;
                    var url = "?id=" + id + "&sid=" + Math.random();
                    my_ajax.onreadystatechange = stateChanged;
                    my_ajax.open("GET", url, true);
                    my_ajax.send(null);
                }
            }

            function do_ajax() {
                if (window.XMLHttpRequest) return new XMLHttpRequest();
                if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
                return null;
            }

            function stateChanged() {
                var n = ids.length;
                var w = wil;
                var data;
                if (my_ajax.readyState == 4) {
                    data = my_ajax.responseText;
                    document.getElementById(w).innerHTML = data.length >= 0 ? data :
                        "<option selected> -- Select City/Region --</option>";
                    <?php foreach($wil as $k=>$w):?>
                    document.getElementById("<?php echo $w[2];?>_box").style.display = (n > <?php echo $k-1;?>) ?
                        'table-row' : 'none';
                    <?php endforeach;?>
                }
            }
    </script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <style>
        .control-label {
            text-align: left!important;
            font-size: 12px;
        }
		#kab_box,#kec_box {
            display:none;
        }
    </style>
    <div class="wrapper">
        <header class="main-header">
            <a href="../index" class="logo">
                <span class="logo-mini">
                    <b>E</b>
                </span>
                <span class="logo-lg">
                    <b>Edwindra</b>.com
                </span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header text-center">ADMIN EDWINDRA</li>
                    <li>
                        <a href="../index">
                            <i class="fa fa-home"></i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li class="treeview active">
                        <a href="#">
                            <i class="fa fa-plus-square"></i>
                            <span>Add Data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="resource">
                                    <i class="fa fa-circle-o"></i> Resource</a>
                            </li>
                            <li class="active">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-circle-o"></i> Career</a>
                            </li>
                            <li>
                                <a href="article">
                                    <i class="fa fa-circle-o"></i> Article</a>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-edit"></i>
                            <span>Edit Data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="../edit/resource">
                                    <i class="fa fa-circle-o"></i> Resource</a>
                            </li>
                            <li>
                                <a href="../edit/career">
                                    <i class="fa fa-circle-o"></i> Career</a>
                            </li>
                            <li>
                                <a href="../edit/article">
                                    <i class="fa fa-circle-o"></i> Article</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </section>
        </aside>
        <div class="content-wrapper">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="fa fa-home"></i> Home
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Add Data</a>
                    </li>
                    <li class="active">Career</li>
                </ol>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="callout callout-info" id="callout-message" style="display:none;"></div>
                            <div class="box box-primary">
                                <div class="box-body pad">
                                    <div class="form-group">
                                        <label for="company" class="col-sm-1 col-xs-12 control-label">Company</label>
                                        <div class="col-sm-5">
                                            <input type="text" id="company" name="company" class="form-control" placeholder="KKP Riza Edwindra" autocomplete="off">
                                        </div>
                                        <label for="email" class="col-sm-1 col-xs-12 control-label">Email</label>
                                        <div class="col-sm-5">
                                            <input type="email" id="email" name="email" class="form-control" placeholder="info@example.com" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="job" class="col-sm-1 col-xs-12 control-label">Job</label>
                                        <div class="col-sm-5">
                                            <input type="text" id="job" name="job" class="form-control" placeholder="Acounting" autocomplete="off">
                                        </div>
                                        <label for="sub_job" class="col-sm-1 col-xs-12 control-label">Sub Job</label>
                                        <div class="col-sm-5">
                                            <input type="text" id="sub_job" name="sub_job" class="form-control" placeholder="Acounting Manager" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="career_level" class="col-sm-1 col-xs-12 control-label">Career Level</label>
                                        <div class="col-sm-5">
                                            <input type="text" id="career_level" name="career_level"class="form-control" autocomplete="off">
                                        </div>
                                        <label for="education_level" class="col-sm-1 col-xs-12 control-label">Education Level</label>
                                        <div class="col-sm-5">
                                            <input type="text" id="education_level" name="education_level" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="years_of_experience" class="col-sm-1 col-xs-12 control-label">Years of Experience</label>
                                        <div class="col-sm-5">
                                            <input type="text" id="years_of_experience" name="years_of_experience" class="form-control" autocomplete="off">
                                        </div>
                                        <label for="minimal_education_description" class="col-sm-1 col-xs-12 control-label">Minimal Education</label>
                                        <div class="col-sm-5">
                                            <input type="text" id="minimal_education_description" name="minimal_education_description" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="job_function" class="col-sm-1 col-xs-12 control-label">Job Function</label>
                                        <div class="col-sm-5">
                                            <textarea type="text" style="resize:vertical;" id="job_function" name="job_function" class="form-control" autocomplete="off"></textarea>
                                        </div>
                                        <label for="experience" class="col-sm-1 col-xs-12 control-label">Minimal Experience</label>
                                        <div class="col-sm-5">
                                            <textarea type="text" style="resize:vertical;" id="experience" name="experience" class="form-control" autocomplete="off"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="title" class="col-sm-1 col-xs-12 control-label">Employment Term</label>
                                        <div class="col-sm-5">
                                            <select id="employment_term" name="employment_term" class="form-control">
                                                <option value="">-- Select --</option>
                                                <option value="1">Full Time</option>
                                                <option value="2">Part Time</option>
                                            </select>
                                        </div>
                                        <label for="title" class="col-sm-1 col-xs-12 control-label">Expired Date</label>
                                        <div class="col-sm-5">
                                            <input type="text" id="datepicker" name="expired_date" class="form-control expired_date" autocomplete="off" placeholder="MM/DD/YY">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="title" class="col-sm-1 col-xs-12 control-label">Work Location</label>
                                        <div class="col-sm-11">
                                            <textarea type="text" style="resize:vertical;" id="work_location" name="work_location" class="form-control" placeholder="Address" autocomplete="off"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-md-offset-1 col-xs-12">
                                            <select id="prov" name="prov" class="form-control" onchange="wilayah(this.value)"  style="margin-bottom:8px;">
                                                <option value="">-- Select Province --</option>
                                                <?php
                                                $query=$db->prepare("SELECT kode,nama FROM wilayah WHERE CHAR_LENGTH(kode)=2 ORDER BY nama");
                                                $query->execute();
                                                while ($data=$query->fetchObject())
                                                echo '<option value="'.$data->kode.'">'.$data->nama.'</option>
                                                ';
					                            ?><select>
                                        </div>
                                        <?php foreach($wil as $w):?>

                                        <div class="col-sm-5 col-xs-12" id='<?php echo $w[2];?>_box'>
                                        <select id="<?php echo $w[2];?>" name="<?php echo $w[2]; ?>" class="form-control" onchange="wilayah(this.value)" style="margin-bottom:8px;">
                                            <option value="">-- Select <?php echo $w[1];?> --</option>
                                        </select>
                                        </div><?php endforeach;?>

                                    </div>
                                    <div class="form-group">
                                        <label for="title" class="col-sm-1 col-xs-12 control-label">Requirements</label>
                                        <div class="col-sm-11">
                                            <textarea class="form-control" id="requirements" name="requirements" placeholder="Place some text here" style="width: 100%; height: 200px;resize:none;"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="title" class="col-sm-1 col-xs-12 control-label">Job Description</label>
                                        <div class="col-sm-11 col-xs-12">
                                            <textarea class="form-control" id="job_description" name="job_description" placeholder="Place some text here" style="width: 100%; height: 200px;resize:none;"></textarea>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="pul-left form-group col-xs-7">
                                            <button class="btn btn-primary" id="btn-submit" style="margin-bottom:8px;">Add Data</button>
                                            <button class="btn btn-primary" id="btn-submit-view" style="margin-bottom:8px;">Add & View Data</button>
                                        </div>
                                        <div class="pull-right form-group col-xs-5 col-md-2">
                                            <button class="btn btn-danger" id="btn-reset" style="margin-bottom:8px;">Reset Form</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <?php include('../item/footer.php'); ?>

    </div>
    <?php include('../item/js.php'); ?>
    <script src="../bower_components/ckeditor/ckeditor.js"></script>
    <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="../item/error.js "></script>
    <script>
        $(document).ready(function () {
            $('#datepicker').datepicker({
                autoclose: true
            });
            CKEDITOR.replace('job_description');
            $("#btn-reset").click(function () {
                $("input").val("");
                $("select").val("");
                $("textarea").val("");
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                    CKEDITOR.instances[instance].setData('');
                }
            });
            $("#btn-submit").click(function() {
                proses();
            });

            $("#btn-submit-view").click(function() {
                proses("view");
            });


            function proses(check) {
                var company = $("#company").val();
                var email = $("#email").val();
                var job = $("#job").val();
                var sub_job = $("#sub_job").val();
                var career_level = $("#career_level").val();
                var education_level = $("#education_level").val();
                var years_of_experience = $("#years_of_experience").val();
                var minimal_education_description = $("#minimal_education_description").val();
                var job_function = $("#job_function").val();
                var experience = $("#experience").val();
                var employment_term = $("#employment_term").val();
                var expired_date = $(".expired_date").val();
                var work_location = $("#work_location").val();
                var prov = $("#prov").val();
                var kab = $("#kab").val();
                var kec = $("#kec").val();
                var requirements = $("#requirements").val();
                var job_description = CKEDITOR.instances.job_description.getData();
                $.ajax({
                    url: '../content/career/add-career.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'company': company,
                        'email': email,
                        'job': job,
                        'sub_job': sub_job,
                        'career_level': career_level,
                        'education_level': education_level,
                        'years_of_experience': years_of_experience,
                        'minimal_education_description': minimal_education_description,
                        'job_function': job_function,
                        'experience': experience,
                        'employment_term': employment_term,
                        'expired_date': expired_date,
                        'work_location': work_location,
                        'prov': prov,
                        'kab': kab,
                        'kec': kec,
                        'requirements': requirements,
                        'job_description': job_description,
                    },
                    beforeSend: function() {
                        $("input").attr('disabled', 'disabled');
                        $("textarea").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete: function() {
                        $("input").removeAttr('disabled');
                        $("textarea").removeAttr('disabled');
                        $("select").removeAttr('disabled');
                        $("button").removeAttr('disabled');
                    },
                    success: function(data) {
                        $("#callout-message").slideDown();
                        if (data.status == 'success') {
                            $("#callout-message").html(data.message).focus();
                            if (check == "view") {
                                setTimeout(function() {
                                        window.location.href = window.location.origin +  "/career";
                                }, 2500);
                            }
                        } else {
                            $("#callout-message").html(data.message).focus();
                        }
                        timeout();
                    },
                    error: errorHandler
                })
            }
        });
    </script>
</body>

</html>

<?php } } ?>
