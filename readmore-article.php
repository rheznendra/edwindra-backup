<?php
   if(!isset($_GET['id'])) {
     header("location:article");
     exit();
   } else {
     include("admin/content/koneksi.php");
     $id = check($_GET['id']);
     $sql = mysqli_query($connect, "SELECT * FROM article WHERE id='$id'");
     if(mysqli_num_rows($sql) == 0) {
       header("location:article");
       exit();
     } else {
       $data = mysqli_fetch_assoc($sql);
       $date = explode(" ", $data['post_date']);
       $tgl = $date[1];
       $month = $date[0];
       $year = $date[2];
     }
     $webtitle = $data['title'] . " || Edwindra";
     $link = "http://".$_SERVER['HTTP_HOST']."/readmore-article?id=".$id;
     if(empty($data['editor'])) {
         $ok = 1;
     } else {
         $ok = 0;
     }
   ?>
  <!DOCTYPE html>
  <html>

  <head>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $webtitle; ?></title>
    <?php include 'item/css.php'; ?>

    <meta property="og:url" content="<?php echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?php echo $webtitle; ?>" />
    <meta property="og:image" content="<?php echo "http://" . $_SERVER['SERVER_NAME']."/public/img/LOGO-KKP-FOR-WEB-BG-hitam.png"; ?>" />
    <meta property="og:description" content="Kantor Konsultan Pajak Riza Edwindra, established in 2006, and  obtain its registered licence in 2010 Started as a small business, we are now located at Jl. Kecubung no 7, Denpasar Timur, with 9 employees. We are registered tax consultant. We committed on giving services to tax payer who has growing demand of tax administration, tax audit, and tax dispute (tax objection and tax appeal), on acquisition and merger and also tax due diligence" />
    <link rel="stylesheet" href="print-js/dist/print.min.css">
  </head>

  <body>
    <style>
      a:link,
      a:visited,
      a:active {
        color: #fff;
      }
    @media only screen and (max-width: 480px) {
    .article-button,
    .article-editor {
        float: <?php echo (empty($data['editor'])?'right!important':'none!important'); ?>;
    }
    }
    </style>
    <nav class="navbar navbar-custom navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header navbar-right">
          <div id="nav-icon3" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
            <span class="icon-bar" style="background-color: #212121"></span>
            <span class="icon-bar" style="background-color: #212121"></span>
            <span class="icon-bar" style="background-color: #212121"></span>
          </div>
          <a class="navbar-brand" href="index">
            <img src="public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
          </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-menu">
          <ul class="nav navbar-nav">
            <li>
              <a href="about">About</a>
            </li>
            <li>
              <a href="services">Services</a>
            </li>
            <li>
              <a href="client">Client</a>
            </li>
            <li>
              <a href="partner">Partner</a>
            </li>
            <li>
              <a href="resource">Resource</a>
            </li>
            <li>
              <a href="career">Career</a>
            </li>
            <li class="active">
              <a href="article">Article</a>
            </li>
            <li>
              <a href="contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="parallax">
      <div class="container">
        <div class="row">
          <p class="text-center wow fadeInDown about-title">Article</p>
        </div>
      </div>
    </div>
    <div class="afexion">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="card" style="border: 2px solid #fff;">
              <div class="article-content">
                  <div style="padding-bottom:30px">
                    <div class="col-md-10 col-sm-8 article-title-readmore" style="line-height:25px;">
                    <div class="text-justify" style="font-weight: bold;"><?php echo $data['title']; ?></div>
                    <div class="posted-by">
                      <i class="fa fa-pencil-square" style="font-size: 12px;"></i> Created By
                      <span class="user">
                        <?php echo $data['creator']; ?>
                      </span>
                    </div>
                    <div class="upload-by">
                      <i class="fa fa-user-circle" style="font-size: 12px;"></i> Posted By
                      <span class="user">
                        <?php echo $data['uploader']; ?>
                      </span>
                    </div>
                  </div>
                    <div class="col-md-2 col-sm-4">
                  <p class="calendar" style="float:right;font-size:14px;vertical-align:bottom;">
                    <i class="fa fa-calendar" style="font-size:15px;"></i>
                    <span class="date">
                      <?php echo $month; ?>
                    </span>
                    <span>
                      <?php echo $tgl; ?>
                    </span>
                    <span>
                      <?php echo $year; ?>
                    </span>
                  </p>
                  </div>
                </div>
                <div class="text-justify article-desc col-md-12">
                  <div id="article_desc">
                    <?php echo $data['description']; ?>
                  </div>
                </div>
                    <?php if(!empty($data['editor'])) { ?>
                    <div class="article-editor">
                      <i class="fa fa-file-text"></i> Edited By
                      <span class="user">
                        <?php echo $data['editor']; ?>
                      </span>at
                      <?php echo $data['edit_date']; ?>
                    </div>
                    <?php } ?>
                    <div class="article-button">
                      <a href="jvascript:void(0);" onclick="open_link('https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>')" data-toggle="tooltip" title="Share Facebook" target="blank">
                        <i class="fa fa-facebook-square" style="color: #fff;"></i>
                      </a>
                      <a href="javascript:void(0);" onclick="open_link('http://twitter.com/share?text=<?php echo urlencode($webtitle); ?>&url=<?php echo $link; ?>')" data-toggle="tooltip" title="Share Twitter" target="blank">
                        <i class="fa fa-twitter-square" style="color: #fff;"></i>
                      </a>
                      <a href="javascript:void(0)" onclick="open_link('https://plus.google.com/share?url=<?php echo $link; ?>')" data-toggle="tooltip" title="Share Google+" target="blank">
                        <i class="fa fa-google-plus-square" style="color: #fff;"></i>
                      </a>
                      <a href="download-article?id=<?php echo $id; ?>" data-toggle="tooltip" title="Download PDF">
                        <i class="fa fa-file-pdf-o" style="color: #fff; font-size: 22px;"></i>
                      </a>
                      <a href="javascript:void(0)" id="copy-link" data-toggle="tooltip" data-link="<?php echo $link; ?>" title="Copy Link" target="blank">
                        <i class="fa fa-link" style="color: #fff; font-size: 22px;"></i>
                      </a>
                      <a href="javascript:void(0)" id="print_file" data-toggle="tooltip" title="Print File">
                        <i class="fa fa-print" style="color: #fff; font-size: 22px;"></i>
                      </a>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include 'item/js.php'; include 'item/footer.php'; ?>
    <script src="clipboard/dist/clipboard.min.js"></script>
    <script src="print-js/dist/print.min.js" charset="utf-8"></script>
    <script>
    function open_link(link) {
        return window.open(link, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }
      $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        if (Clipboard.isSupported() != true) {
          $("#copy-link").hide();
        }
        $("#print_file").click(function () {
          printJS({
            printable: 'article_desc',
            type: 'html',
            header: '<?php echo $data['title']; ?>',
            documentTitle: '<?php echo $webtitle; ?>',
            headerStyle: 'font-weight:bold;text-align:center;',
          });
        })
        new Clipboard('#copy-link', {
          text: function (trigger) {
            return trigger.getAttribute('data-link');
          }
        });
      });
    </script>
  </body>

  </html>
  <?php } ?>