<?php
    if ( $_SERVER['REQUEST_METHOD']=='GET' && realpath(__FILE__) == realpath( $_SERVER['SCRIPT_FILENAME'] ) ) {
        header( 'HTTP/1.0 403 Forbidden', TRUE, 403 );
        die( header( 'location: /' ) );
        exit();

    } else {
?>
<!DOCTYPE html>
<html itemscope>

<head>
    <title>403 Forbidden || KKP Riza Edwindra</title>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php $site_name = "403 Forbidden || KKP Riza Edwindra"; include '../item/css.php'; include("../item/open_graph.php"); ?>

</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header navbar-right">
                <div id="nav-icon" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                </div>
            <a class="navbar-brand" href="index">
              <img src="../public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
            </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-menu" style="margin-top:10px;">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="../about">About</a>
                    </li>
                    <li>
                        <a href="../services">Services</a>
                    </li>
                    <li>
                        <a href="../client">Client</a>
                    </li>
                    <li>
                        <a href="../partner">Partner</a>
                    </li>
                    <li>
                        <a href="../resource">Resource</a>
                    </li>
                    <li>
                        <a href="../career">Career</a>
                    </li>
                    <li>
                        <a href="../article">Article</a>
                    </li>
                    <li>
                        <a href="../contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="parallax2">
        <div class="container">
            <div class="row">
                <p class="text-center animated fadeInDown about-title">403 Forbidden</p>
            </div>
        </div>
    </div>
    <?php
      include '../item/footer.php';
      include '../item/js.php';
    ?>
</body>

</html>
<?php } ?>