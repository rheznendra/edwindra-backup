<!DOCTYPE html>
<html itemscope>

<head>
    <title>About || KKP Riza Edwindra</title>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php $site_name = "About || KKP Riza Edwindra"; include 'item/css.php'; include("item/open_graph.php"); ?>

</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header navbar-right">
                <div id="nav-icon" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                </div>
            <a class="navbar-brand" href="index">
              <img src="public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
            </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-menu" style="margin-top:10px;">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="about">About</a>
                    </li>
                    <li>
                        <a href="services">Services</a>
                    </li>
                    <li>
                        <a href="client">Client</a>
                    </li>
                    <li>
                        <a href="partner">Partner</a>
                    </li>
                    <li>
                        <a href="resource">Resource</a>
                    </li>
                    <li>
                        <a href="career">Career</a>
                    </li>
                    <li>
                        <a href="article">Article</a>
                    </li>
                    <li>
                        <a href="contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="parallax">
        <div class="container">
            <div class="row">
                <p class="text-center animated fadeInDown about-title">About Us</p>
            </div>
        </div>
    </div>
    <div class="afexion">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="public/img/tax-about.jpg" class="img-responsive">
                </div>
                <div class="col-md-6 text-justify">
                    <p class="about-caption">
                        Kantor Konsultan Pajak Riza Edwindra
                    </p>
                    <p class="about-item">
                        Kantor Konsultan Pajak Riza Edwindra, established in 2006, and obtain its registered license in 2010.
                    </p>
                    <p class="about-item">
                        Started as a small business, we are now located at Jl. Kecubung no 7, Denpasar Timur, with 9 employees. We are registered tax consultant.
                    </p>
                    <p class="about-item">
                        We committed on giving services to tax payer who has growing demand of tax administration, tax audit, and tax dispute (tax objection and tax appeal), on acquisition and merger and also tax due diligence.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <?php
      include 'item/footer.php';
      include 'item/js.php';
    ?>
</body>

</html>