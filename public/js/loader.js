function delete_resource() {
    $.ajax({
        url: '../content/resource/delete-resource.php',
        type: 'POST',
        dataType: 'json',
        data: {
            'title': title,
            'link': link,
            'id': id,
        },
        beforeSend: function() {
            $("input").attr('disabled', 'disabled');
            $("textarea").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete: function() {
            $("input").removeAttr('disabled');
            $("textarea").removeAttr('disabled');
            $("button").removeAttr('disabled');
        },
        success: function(data) {
            $("#callout-message").slideDown();
            if (data.status == 'success') {
                $("#callout-message").html(data.message).focus();
            } else {
                $("#callout-message").html(data.message).focus();
            }
            timeout();
        },
        error: errorHandler
    })
}

function timeout() {
    setTimeout(function() {
        $("#callout-message").slideUp();
    }, 5000);
}

function errorHandler(jqXHR, exception) {
    $("#callout-message").slideDown();
    if (jqXHR.status === 0) {
        $("#callout-message").html('Not connect.\n Verify Network.').focus();
    } else if (jqXHR.status == 404) {
        $("#callout-message").html('Requested page not found. [404]').focus();
    } else if (jqXHR.status == 500) {
        $("#callout-message").html('Internal Server Error [500].').focus();
    } else if (exception === 'parsererror') {
        $("#callout-message").html('Requested JSON parse failed.');
    } else if (exception === 'timeout') {
        $("#callout-message").html('Time out error.').focus();
    } else if (exception === 'abort') {
        $("#callout-message").html('Ajax request aborted.').focus();
    } else {
        $("#callout-message").html('Uncaught Error.\n' + jqXHR.responseText).focus();
    }
    timeout();
}