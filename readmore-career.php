<?php
include("admin/content/koneksi.php");
$job = explode("/", urldecode(check($_GET['job'])))[0];
$job = explode(".php",$job)[0];
$company = str_replace("-", " ", urldecode(check($_GET['company'])));
$company = explode(".php",$company)[0];
$sql = mysqli_query($connect, "SELECT * FROM career WHERE job = '$job' AND company = '$company'");
if(mysqli_num_rows($sql) == 0) {
    echo $_GET['company'];
    exit();
}
$data = mysqli_fetch_assoc($sql);
$webtitle = $job." - ". $company . " || Edwindra";
$descog = strip_tags($data['job_description']);
if(strlen($descog) > 100) {
    $descog = substr($descog,0,150)."...";
}
$date = explode(" ", $data['post_date']);
$month = $date[0];
if($month == "Jan") {
    $month = "January";
} else if($month == "Feb") {
    $month = "February";
} else if($month == "Mar") {
    $month = "March";
} else if($month == "Apr") {
    $month = "April";
} else if($month == "Jun") {
    $month = "June";
} else if($month == "Jul") {
    $month = "July";
} else if($month == "Aug") {
    $month = "August";
} else if($month == "Sep") {
    $month = "September";
} else if($month == "Oct") {
    $month = "October";
} else if($month == "Nov") {
    $month = "November";
} else if($month == "Dec") {
    $month = "December";
}
$date = $month. ", " . $date[1]. " " . $date[2];
?>
    <!DOCTYPE html>
    <html>

    <head>
        <link rel="shortcut icon" href="../../../public/img/favicon.ico" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $webtitle; ?></title>
        <meta property="og:url" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']; ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php echo $webtitle; ?>" />
        <meta property="og:description" content="<?php echo $descog; ?>" />
        <meta property="og:image" content="https://www.your-domain.com/path/image.jpg" />
        <?php include 'item/css.php'; ?> </head>

    <body>
        <nav class="navbar navbar-custom navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header navbar-right">
                    <div id="nav-icon" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <span class="icon-bar" style="background-color: #212121"></span>
                        <span class="icon-bar" style="background-color: #212121"></span>
                        <span class="icon-bar" style="background-color: #212121"></span>
                    </div>
                    <a class="navbar-brand" href="index">
                    <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
                </a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-menu" style="margin-top:10px;">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="../../../about">About</a>
                        </li>
                        <li>
                            <a href="../../../services">Services</a>
                        </li>
                        <li>
                            <a href="../../../client">Client</a>
                        </li>
                        <li>
                            <a href="../../../partner">Partner</a>
                        </li>
                        <li>
                            <a href="../../../resource">Resource</a>
                        </li>
                        <li class="active">
                            <a href="../../../career">Career</a>
                        </li>
                        <li>
                            <a href="../../../article">Article</a>
                        </li>
                        <li>
                            <a href="../../../contact">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="parallax">
            <div class="container">
                <div class="row">
                    <p class="text-center animated fadeInDown about-title">Career</p>
                    <p class="text-center animated fadeInDown" style="color: #fff; font-weight: lighter;font-size: 30px;">Our client is looking Jobs Hiring for You!</p>
                </div>
            </div>
        </div>
        <div class="afexion">
            <div class="container">
                <div class="card career-background">
                    <div class="row">
                        <div class="col-md-8 col-xs-12" style="padding-top: 10px;">
                            <div class="career-public">
                                <div class="career-header">
                                    <span class="company-name"><?php echo $data['company']; ?></span>
                                    <div class="pull-right date-post">
                                        <div class="posted">Posted on</div>
                                        <div class="posted-date"> <?php echo $date; ?> </div>
                                    </div>
                                </div>
                                <div class="career-content">
                                    <div class="career-job">
                                        <?php echo $data['job']; ?>
                                    </div>
                                    <div class="jobs">
                                        <i class="fa fa-briefcase"></i>
                                        <?php echo $data['sub_job']; ?>
                                    </div>
                                    <div class="career-description">
                                        <span class="title-description">Job Description :</span>
                                        <?php echo $data['job_description']; ?>
                                    </div>
                                    <div class="career-requirement">
                                        <span class="requirement"> Requirements :</span>
                                        <ul>
                                            <?php echo $data['requirements']; ?>
                                        </ul>
                                    </div>
                                    <div style="margin:24px 0;">
                                        <div class="divTable">
                                            <div class="divTableBody">
                                                <div class="divTableRow">
                                                    <div class="divTableCell" style="width:40%;">Career Level </div>
                                                    <div class="divTableCell" style="width:60%;">
                                                        <?php echo $data['career_level']; ?>
                                                    </div>
                                                </div>
                                                <div class="divTableRow">
                                                    <div class="divTableCell" style="width:40%;">Yr(s) of Experience</div>
                                                    <div class="divTableCell" style="width:60%;">
                                                        <?php echo $data['year_experience']; ?>
                                                    </div>
                                                </div>
                                                <div class="divTableRow">
                                                    <div class="divTableCell" style="width:40%;">Education Level</div>
                                                    <div class="divTableCell" style="width:60%;">
                                                        <?php echo $data['education_level']; ?>
                                                    </div>
                                                </div>
                                                <div class="divTableRow">
                                                    <div class="divTableCell" style="width:40%;">Job Function</div>
                                                    <div class="divTableCell" style="width:60%;">
                                                        <?php echo $data['job_function']; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="job-right-top">
                                <span><?php echo $data['sub_job']; ?> :</span>
                                <ul>
                                    <li>
                                        <?php echo $data['minimal_education_description']; ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="job-right">
                                <i class="fa fa-map-marker"></i> Work Location :
                                <ul style="list-style-type: none;">
                                    <li>
                                        <?php echo $data['work_location']. " " . $data['kec'] . " " . $data['kab'] . " " .$data['prov']; ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="job-right">
                                <i class="fa fa-clock-o"></i> Employment Term :
                                <ul style="list-style-type: none;">
                                    <li>
                                        <?php echo $data['employment_term']; ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="job-right">
                                <i class="fa fa-briefcase"></i> Experience :
                                <ul>
                                    <?php echo $data['experience']; ?>
                                </ul>
                            </div>
                            <div class="job-right-bottom">
                                <div>
                                    Send your complete CV to :
                                    <div style="padding-bottom: 10px;margin-left:6px;">
                                        <i class="fa fa-envelope"></i>
                                        <a href="mailto:<?php echo $data['email'] ?>" style="color: #fff;">
                                            <?php echo $data['email']; ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'item/footer.php'; include 'item/js.php'; ?>
    </body>

    </html>
