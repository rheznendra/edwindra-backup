<!DOCTYPE html>
<html>

<head>
    <title>Contact || KKP Riza Edwindra</title>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php $site_name = "Contact || KKP Riza Edwindra"; include 'item/css.php'; include("item/open_graph.php"); ?>

</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header navbar-right">
                <div id="nav-icon" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                </div>
            <a class="navbar-brand" href="index">
              <img src="public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
            </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-menu" style="margin-top:10px;">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about">About</a>
                    </li>
                    <li>
                        <a href="services">Services</a>
                    </li>
                    <li>
                        <a href="client">Client</a>
                    </li>
                    <li>
                        <a href="partner">Partner</a>
                    </li>
                    <li>
                        <a href="resource">Resource</a>
                    </li>
                    <li>
                        <a href="career">Career</a>
                    </li>
                    <li>
                        <a href="article">Article</a>
                    </li>
                    <li class="active">
                        <a href="contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="parallax">
        <div class="container">
            <div class="row">
                <p class="text-center wow fadeInDown about-title" style="">Contact Us</p>
            </div>
        </div>
    </div>
    <div class="afexion">
        <div class="container" style="margin-bottom: 50px; margin-top: 50px">
            <div class="row">
                <div class="col-md-6">
                    <div class="google-maps">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d986.1095365828041!2d115.22936092915698!3d-8.649811599611526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd240821ba7c189%3A0xc7dbd1dbf7609e1d!2sKantor+Konsultan+Pajak+Riza+Edwindra!5e0!3m2!1sen!2sid!4v1510538820409" width="400" height="300" frameborder="0" style="border:0" allowfullscreen>
            </iframe>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control email-form" id="email" placeholder="Email" style="width: 100%">
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <input type="name" name="name" class="form-control" id="nama" placeholder="Name" style="width: 100%">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="name" name="subject" class="form-control" id="subject" placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="content" id="message" placeholder="Message" rows="6"></textarea>
                    </div>
                    <button type="submit" class="btn btn-custom btn-block" onclick="sendMail()" style="width:100%;">Send</button>
                </div>
            </div>
        </div>
    </div>
    <?php 
      include 'item/footer.php';
      include 'item/js.php'; 
    ?>
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>

</html>