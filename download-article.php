<?php
require_once 'vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
if(!isset($_GET['id'])) {
    header("location:article");
    exit();
} else {
    include("admin/content/koneksi.php");
    $id = check($_GET['id']);
    $sql = mysqli_query($connect, "SELECT * FROM article WHERE id='$id'");
    $data = mysqli_fetch_assoc($sql);
    if(mysqli_num_rows($sql) == 0 || empty($data['description'])) {
        header("location:article");
        exit();
    } else {
        try {
            ob_start();
            $content = ob_get_clean();
            $content = '<page><h2 style="text-align:center;">'.$data['title'].'</h2>'.$data['description'].'</page>';
            $html2pdf = new Html2Pdf('P', 'A4', 'en', true, 'UTF-8');
            $html2pdf->writeHTML($content);
            $html2pdf->output($data['title'].'.pdf','D');
        } catch (Html2PdfException $e) {
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }
    }
}
