<!DOCTYPE html>
<html>

<head>
    <title>Client || KKP Riza Edwindra</title>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php $site_name = "Client || KKP Riza Edwindra"; include 'item/css.php'; include("item/open_graph.php"); ?>

</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header navbar-right">
                <div id="nav-icon" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                </div>
                <a class="navbar-brand" href="index">
                    <img src="public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-menu" style="margin-top:10px;">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about">About</a>
                    </li>
                    <li>
                        <a href="services">Services</a>
                    </li>
                    <li class="active">
                        <a href="client">Client</a>
                    </li>
                    <li>
                        <a href="partner">Partner</a>
                    </li>
                    <li>
                        <a href="resource">Resource</a>
                    </li>
                    <li>
                        <a href="career">Career</a>
                    </li>
                    <li>
                        <a href="article">Article</a>
                    </li>
                    <li>
                        <a href="contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="parallax">
        <div class="container">
            <div class="row">
                <p class="text-center wow fadeInDown client-title">Our Client</p>
            </div>
        </div>
    </div>
    <div class="afexion" style="color:#ffffff;">
        <div class="client">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="text-center client-caption">Our Clients consist of Companies and businesses on :</p>
                    </div>
                    <div class="col-sm-12 text-center">
                        <div class="row">
                            <div class="col-sm-3 client-icon">
                                <i class="fa fa-bed" style="font-size: 45px;margin-bottom: 20px;" aria-hidden="true"></i>
                                <p style="font-size: 20px">Hotel & Villa Management</p>
                            </div>
                            <div class="col-sm-3 client-icon">
                                <i class="fa fa-cutlery" style="font-size: 45px;margin-bottom: 20px;" aria-hidden="true"></i>
                                <p style="font-size: 20px">Restaurant</p>
                            </div>
                            <div class="col-sm-3 client-icon">
                                <i class="fa fa-industry" style="font-size: 45px;margin-bottom: 20px;" aria-hidden="true"></i>
                                <p style="font-size: 20px">Manufaktur</p>
                            </div>
                            <div class="col-sm-3 client-icon">
                                <i class="fa fa-shopping-cart" style="font-size: 45px;margin-bottom: 20px;" aria-hidden="true"></i>
                                <p style="font-size: 20px">Retail, Trading and Export</p>
                            </div>
                            <div class="margin-client">
                                <div class="col-sm-3 client-icon">
                                    <i class="fa fa-building-o" style="font-size: 45px;margin-bottom: 20px;" aria-hidden="true"></i>
                                    <p style="font-size: 20px">Property Agent</p>
                                </div>
                                <div class="col-sm-3 client-icon">
                                    <i class="fa fa-bus" style="font-size: 45px;margin-bottom: 20px;" aria-hidden="true"></i>
                                    <p style="font-size: 20px">Tour & Travel</p>
                                </div>
                                <div class="col-sm-3 client-icon">
                                    <i class="fa fa-wrench" style="font-size: 45px;margin-bottom: 20px;" aria-hidden="true"></i>
                                    <p style="font-size: 20px">Construction & Design</p>
                                </div>
                                <div class="col-sm-3">
                                    <i class="fa fa-truck" style="font-size: 45px;margin-bottom: 20px;" aria-hidden="true"></i>
                                    <p style="font-size: 20px">Distributor & Wholesale</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php 
      include 'item/footer.php';
      include 'item/js.php'; 
    ?>
</body>

</html>