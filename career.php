<?php
include("admin/content/koneksi.php");
$query = mysqli_query($connect, "SELECT * FROM career ORDER BY id DESC");
?>
    <!DOCTYPE html>
    <html>

    <head>
    <title>Career || KKP Riza Edwindra</title>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php $site_name = "Career || KKP Riza Edwindra"; include 'item/css.php'; include("item/open_graph.php"); ?>

    <body>
        <nav class="navbar navbar-custom navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header navbar-right">
                    <div id="nav-icon" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <span class="icon-bar" style="background-color: #212121"></span>
                        <span class="icon-bar" style="background-color: #212121"></span>
                        <span class="icon-bar" style="background-color: #212121"></span>
                    </div>
                    <a class="navbar-brand" href="index">
                        <img src="public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-menu" style="margin-top:10px;">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="about">About</a>
                        </li>
                        <li>
                            <a href="services">Services</a>
                        </li>
                        <li>
                            <a href="client">Client</a>
                        </li>
                        <li>
                            <a href="partner">Partner</a>
                        </li>
                        <li>
                            <a href="resource">Resource</a>
                        </li>
                        <li class="active">
                            <a href="career">Career</a>
                        </li>
                        <li>
                            <a href="article">Article</a>
                        </li>
                        <li>
                            <a href="contact">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="parallax">
            <div class="container">
                <div class="row">
                    <p class="text-center animated fadeInDown about-title">Career</p>
                </div>
            </div>
        </div>
        <div class="afexion">
            <div class="container">
                <div class="row">
                    <div id="easyPaginate" class="col-md-12">
                        <?php
if(mysqli_num_rows($query) == 0) {
    echo "<div class='error'><h3>Tidak ada data untuk ditampilkan</h3></div>";
} else {
    while($row = mysqli_fetch_assoc($query)) {
    $employment_term = $row['employment_term'];
    $company = urlencode($row['company']);
    $job = urlencode($row['job']);
    $webtitle = $job." - ". $company . urlencode(" || Edwindra\n");
    $description = strip_tags($row['job_description']);
    $link = "http://".$_SERVER['HTTP_HOST']."/career/read/". $job ."/". $company;
    if(strlen($description) > 200) {
        $description = substr($description,0,200)."...<a href=".$link." title=".$row['company']." class='read-more'> Read More</a>";
    } else {
        $description = substr($description,0,200);
    }
    $date = explode(" ", $row['post_date']);
    $month = $date[0];
    if($month == "Jan") {
        $month = "January";
    } else if($month == "Feb") {
        $month = "February";
    } else if($month == "Mar") {
        $month = "March";
    } else if($month == "Apr") {
        $month = "April";
    } else if($month == "Jun") {
        $month = "June";
    } else if($month == "Jul") {
        $month = "July";
    } else if($month == "Aug") {
        $month = "August";
    } else if($month == "Sep") {
        $month = "September";
    } else if($month == "Oct") {
        $month = "October";
    } else if($month == "Nov") {
        $month = "November";
    } else if($month == "Dec") {
        $month = "December";
    }
    $date = $month. ", " . $date[1]. " " . $date[2];
    $kab = $row['kab'];
    $prov = $row['prov'];
    if(empty($kab)) {
        $lokasi = $prov;
    } else if(empty($prov)) {
        $lokasi = $kab;
    } else if(!empty($kab) && !empty($prov)) {
        $lokasi = $row['kab']. ", " . $row['prov'];
    }
?>
                            <div class="col-md-6">
                                <div class="bg-table">
                                    <div class="career-title">
                                        <p class="career-post-link">
                                            <a href="<?php echo $link; ?>" style="color: #000;">
                                                <?php echo $row['job']; ?>
                                            </a>
                                        </p>
                                    </div>
                                    <div class="career-entry">
                                        <p class="career-company">
                                            <?php echo $row['company']; ?>
                                        </p>
                                        <div class="career-time">
                                            <p class="<?php echo ($employment_term == 'Full Time')?'work-fulltime':'work-parttime'; ?>">
                                                <i class="fa fa-clock-o"></i>
                                                <?php echo $row['employment_term']; ?>
                                            </p>
                                        </div>
                                        <div class="career-place">
                                            <p> -
                                                <i class="fa fa-map-marker"></i>
                                                <?php echo $lokasi; ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="entry-summary">
                                        <p class="caption">
                                            <?php echo $row['company']; ?> -
                                            <?php echo $description; ?>
                                        </p>
                                    </div>
                                    <div>
                                        <div class="career-post-time">
                                            <i class="fa fa-calendar"></i>
                                            <?php echo $date; ?>
                                            <div class="pull-right social-btn">
                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" data-toggle="tooltip" target="_blank" title="Share Facebook">
                                                    <i class="fa fa-facebook-square"></i>
                                                </a>
                                                <a href="http://twitter.com/share?text=<?php echo $webtitle; ?>&url=<?php echo $link; ?>" data-toggle="tooltip" target="_blank"
                                                    title="Share Twitter">
                                                    <i class="fa fa-twitter-square"></i>
                                                </a>
                                                <a href="https://plus.google.com/share?url=<?php echo $link; ?>" data-toggle="tooltip" target="_blank" title="Share Google+">
                                                    <i class="fa fa-google-plus-square"></i>
                                                </a>
                                                <a href="javascript:void(0)" data-link="<?php echo $link; ?>" id="copy-link" data-toggle="tooltip" target="_blank" title="Click to Copy!">
                                                    <i class="fa fa-chain"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'item/footer.php'; include 'item/js.php'; ?>
        <script src="public/js/jquery.easyPaginate.js"></script>
        <script src="clipboard/dist/clipboard.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#easyPaginate').easyPaginate({
                    paginateElement: 'div.col-md-6',
                    elementsPerPage: 4,
                    effect: 'default'
                });
                if(Clipboard.isSupported() != true) {
                  $("#copy-link").hide();
                }
                new Clipboard('#copy-link', {
                    text: function(trigger) {
                        return trigger.getAttribute('data-link');
                    }
                });
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </body>

    </html>
