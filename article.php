<?php
include('admin/content/koneksi.php');
$sql = mysqli_query($connect, "SELECT * FROM article ORDER BY id DESC");
?>
    <!DOCTYPE html>
    <html>

    <head>
    <title>Article || KKP Riza Edwindra</title>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php $site_name = "Article || KKP Riza Edwindra"; include 'item/css.php'; include("item/open_graph.php"); ?>
        <link rel="stylesheet" href="public/css/article.css">

    </head>

    <body>

        <nav class="navbar navbar-custom navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header navbar-right">
                    <div id="nav-icon3" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <span class="icon-bar" style="background-color: #212121"></span>
                        <span class="icon-bar" style="background-color: #212121"></span>
                        <span class="icon-bar" style="background-color: #212121"></span>
                    </div>
                    <a class="navbar-brand" href="index">
                        <img src="public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="about">About</a>
                        </li>
                        <li>
                            <a href="services">Services</a>
                        </li>
                        <li>
                            <a href="client">Client</a>
                        </li>
                        <li>
                            <a href="partner">Partner</a>
                        </li>
                        <li>
                            <a href="resource">Resource</a>
                        </li>
                        <li>
                            <a href="career">Career</a>
                        </li>
                        <li class="active">
                            <a href="article">Article</a>
                        </li>
                        <li>
                            <a href="contact">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="parallax">
            <div class="container">
                <div class="row">
                    <p class="text-center wow fadeInDown about-title">Article</p>
                </div>
            </div>
        </div>
        <div class="afexion">
            <div class="container">
                <div class="row">
                    <div id="easyPaginate" class="col-md-12 col-xs-12">
                        <?php
            if(mysqli_num_rows($sql) == 0) {
                echo "<div class='error'><h3>Tidak ada data untuk ditampilkan</h3></div>";
            } else {
                while($row = mysqli_fetch_assoc($sql)) {
                if($row['type'] == 1) {
                    $desc = $row['short_desc'];
                    $link = "pdf.js/web/viewer.php?file=".urlencode("http://" . $_SERVER['SERVER_NAME']."/pdf/".$row['file_name']);
                } else {
                    $desc = $row['description'];
                    $link = "readmore-article?id=".$row['id'];
                }
                if(strlen($desc) > 200) {
                    $desc = strip_tags(substr($desc,0,200));
                } else {
                    $desc = strip_tags(substr($desc,0,200));
                }
                if(strlen($row['title']) > 80) {
                    $title = strip_tags(substr($row['title'],0,80))."...";
                } else {
                    $title = strip_tags(substr($row['title'],0,80));
                }
                $date = explode(" ", $row['post_date']);
                $tgl = $date[1];
                $month = $date[0];
                $year = $date[2];
            ?>
                            <div class="col-md-6 col-xs-12">
                                <div class="card">
                                    <div class="article-content">
                                        <div class="article-title" style="line-height:25px;">
                                            <div class="text-justify col-md-8">
                                                <div class="ini-judul">
                                                <?php echo $title; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="calendar-article" style="font-size:14px;vertical-align:bottom;">
                                                    <span class="date-post-article">
                                                        <i class="fa fa-calendar"></i>
                                                        <?php echo $month; ?>
                                                    </span>
                                                    <span>
                                                        <?php echo $tgl; ?>
                                                    </span>
                                                    <span>
                                                        <?php echo $year; ?>
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                        <div class="text-justify article-desc" style="height:90px;">
                                            <?php echo $desc; ?>
                                        </div>
                                        <div class="article-btn button-read-more">
                                            <a class="btn btn-default" target="_blank" href="<?php echo $link; ?>">Read More...</a>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php }
            }
            ?>
                    </div>
                </div>
            </div>
        </div>

        <?php
      include 'item/js.php';
      include 'item/footer.php';
    ?>
            <script src="public/js/jquery.easyPaginate.js"></script>
            <script>
                $(document).ready(function () {
                    $('#easyPaginate').easyPaginate({
                        paginateElement: 'div.col-md-6',
                        elementsPerPage: 4,
                        effect: 'default'
                    });
                });
            </script>
    </body>

    </html>