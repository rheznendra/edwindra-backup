<!DOCTYPE html>
<html>

<head>
    <title>Partner || KKP Riza Edwindra</title>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php $site_name = "Partner || KKP Riza Edwindra"; include 'item/css.php'; include("item/open_graph.php"); ?>
</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header navbar-right">
                <div id="nav-icon" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                </div>
                <a class="navbar-brand" href="index">
                    <img src="public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-menu" style="margin-top:10px;">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about">About</a>
                    </li>
                    <li>
                        <a href="services">Services</a>
                    </li>
                    <li>
                        <a href="client">Client</a>
                    </li>
                    <li class="active">
                        <a href="partner">Partner</a>
                    </li>
                    <li>
                        <a href="resource">Resource</a>
                    </li>
                    <li>
                        <a href="career">Career</a>
                    </li>
                    <li>
                        <a href="article">Article</a>
                    </li>
                    <li>
                        <a href="contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="parallax">
        <div class="container">
            <div class="row">
                <p class="text-center wow fadeInDown about-title" style="">Our Partner</p>
            </div>
        </div>
    </div>
    <div class="afexion">
        <div class="container" style="margin-bottom: 50px;">
            <div class="row">
                <div class="col-md-3" style="padding:0px;padding:10px; margin-top: -10px;">
                    <img src="public/img/team/riza.jpg?version=2" class="img-responsive" width="100%" height="100%">
                </div>
                <div class="col-md-9">
                    <div class="tab-card">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#bio" aria-controls="bio" role="tab" data-toggle="tab">Bio</a>
                            </li>
                            <li role="presentation">
                                <a href="#education" aria-controls="education" role="tab" data-toggle="tab">Education</a>
                            </li>
                            <li role="presentation">
                                <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Professional Career</a>
                            </li>
                            <li role="presentation">
                                <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Certification and Registration</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="bio">
                                <p class="partner-caption">
                                    Riza Edwindra, SE, MSi, Ak, CA
                                </p>
                                <div class="divTable">
                                    <div class="divTableBody" style="font-size:16px">
                                        <div class="divTableRow">
                                            <div class="divTableCell">Place of Birth</div>
                                            <div class="divTableCell">
                                            : Surabaya
                                            </div>
                                        </div>
                                        <div class="divTableRow">
                                            <div class="divTableCell">Date of Birth</div>
                                            <div class="divTableCell">
                                            : 26 February 1974
                                            </div>
                                        </div>
                                        <div class="divTableRow">
                                            <div class="divTableCell">Email</div>
                                            <div class="divTableCell">
                                            : riza@edwindra.com
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="education">
                                <p class="partner-caption">
                                    Education
                                </p>
                                <table>
                                    <tr>
                                        <td style="font-size: 14px">- Economic Faculty - Accounting, Brawijaya University</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Magister Accounting, Udayana University</td>
                                    </tr>
                                    </tr>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="messages">
                                <p class="partner-caption">
                                    Profesional Career
                                </p>
                                <table>
                                    <tr>
                                        <td style="font-size: 14px">- Junior Audit – KPMG Hanadi Sujendro</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Senior Audit – Arthur Andersen Prasetio Utomo</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Senior Audit – Ernst & Young Prasetio Sarwoko Sandjaja</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- General Manager – PT Instant Print Pratama</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Senior Manager Account and Administration – PT Dayak Worldwide Utama</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Tax Officer – CV Bali Balance</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Managing Partner – Primandiri Consulting</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Managing Partner - Kantor Konsultan Pajak Riza Edwindra</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Managing Partner - Edwindra & Partner</td>
                                    </tr>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="settings">
                                <p class="partner-caption">
                                    Certification and Registration
                                </p>
                                <table>
                                    <tr>
                                        <td style="font-size: 14px">- Tax Consultant Certification A SI-1783/PJ/2010</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Tax Consultant Certification B SI-2182/PJ/2012</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Tax Consultant Certification C SI-2679/PJ/2013</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Tax Consultant Registration No. KEP 468/IP.C/PJ/2015</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Tax Court Registration No. 563/PP/IKH/2015</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Register Negara Akuntan No. D-19.740</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Certified Accountant No. 11.D19740</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3" style="padding:10px; margin-top: -10px;">
                    <img src="public/img/team/kiki.jpg?version=1" class="img-responsive" width="100%">
                </div>
                <div class="col-md-9">
                    <div class="tab-card">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#bio2" aria-controls="bio" role="tab" data-toggle="tab">Bio</a>
                            </li>
                            <li role="presentation">
                                <a href="#education2" aria-controls="education" role="tab" data-toggle="tab">Education</a>
                            </li>
                            <li role="presentation">
                                <a href="#messages2" aria-controls="messages" role="tab" data-toggle="tab">Professional Career</a>
                            </li>
                            <li role="presentation">
                                <a href="#settings2" aria-controls="settings" role="tab" data-toggle="tab">Certification and Registration</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="bio2">
                                <p class="partner-caption">
                                    Ida Ayu Niki Safitri, SE
                                </p>
                                <div class="divTable">
                                    <div class="divTableBody" style="font-size:16px">
                                        <div class="divTableRow">
                                            <div class="divTableCell">Place of Birth</div>
                                            <div class="divTableCell">
                                            : Banyuwangi
                                            </div>
                                        </div>
                                        <div class="divTableRow">
                                            <div class="divTableCell">Date of Birth</div>
                                            <div class="divTableCell">
                                            : 07 Mei 1989
                                            </div>
                                        </div>
                                        <div class="divTableRow">
                                            <div class="divTableCell">Email</div>
                                            <div class="divTableCell">
                                            : dayu.niki@edwindra.com
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="education2">
                                <p class="partner-caption">
                                    Education
                                </p>
                                <table>
                                    <tr>
                                        <td style="font-size: 14px">- Economic Faculty – Accounting, Warmadewa University</td>
                                    </tr>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="messages2">
                                <p class="partner-caption">
                                    Profesional Career
                                </p>
                                <table>
                                    <tr>
                                        <td style="font-size: 14px">- Junior Staf – Kantor Konsultan Pajak Riza Edwindra</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Senior Staff – Kantor Konsultan Pajak Riza Edwindra</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 14px">- Junior Partner – Edwindra & Partner</td>
                                    </tr>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="settings2">
                                <p class="partner-caption">
                                    Certification and Registration
                                </p>
                                <table>
                                    <tr>
                                        <td style="font-size: 14px">- Tax Consultant Registration No. KEP-1698/IP.A/PJ/2015</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php 
      include 'item/js.php'; 
      include 'item/footer.php';
    ?>
</body>

</html>