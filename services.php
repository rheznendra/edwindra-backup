<!DOCTYPE html>
<html>

<head>
    <title> Services || KKP Riza Edwindra </title>
    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php $site_name = "Services || KKP Riza Edwindra"; include 'item/css.php'; include("item/open_graph.php"); ?>

</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header navbar-right">
                <div id="nav-icon" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                    <span class="icon-bar" style="background-color: #212121"></span>
                </div>
                <a class="navbar-brand" href="index">
                    <img src="public/img/LOGO-KKP-FOR-WEB-font-putih-resize.png" height="110">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-menu" style="margin-top:10px;">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about">About</a>
                    </li>
                    <li class="active">
                        <a href="services">Services</a>
                    </li>
                    <li>
                        <a href="client">Client</a>
                    </li>
                    <li>
                        <a href="partner">Partner</a>
                    </li>
                    <li>
                        <a href="resource">Resource</a>
                    </li>
                    <li>
                        <a href="career">Career</a>
                    </li>
                    <li>
                        <a href="article">Article</a>
                    </li>
                    <li>
                        <a href="contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="parallax">
        <div class="container">
            <div class="row">
                <p class="text-center wow fadeInDown services-title" style="">Our Services</p>
            </div>
        </div>
    </div>

    <div class="afexion">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="card">
                        <div class="card-image">
                            <img class="img-responsive img-services" src="public/img/services/Tax Administration.jpg">
                        </div>

                        <div class="card-content">
                            <h4 class="title-card">Tax Administration</h4>
                            <p class="text-justify">
                                Our Services on tax administration consist of tax calculation, tax report and tax filling, with the objective to report tax
                                correctly, accurate and timely.
                            </p>
                            <!-- <p class="text-justify">Our Services on tax administration consist of tax calculation, tax report and tax filling.... <a href="#" data-toggle="modal" data-target="#taxtAdmin">Readmore</a></p> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="card">
                        <div class="card-image">
                            <img class="img-responsive img-services" src="public/img/services/Tax Training.jpg">
                        </div>

                        <div class="card-content">
                            <h4 class="title-card">Tax Training</h4>
                            <p class="text-justify">
                                Tax training for tax payer and tax payer employee, so they can increase their capabilities and knowledge of the employee,
                                with the objective increase of tax compliance
                                <br>
                                <br>
                                <br>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="card">
                        <div class="card-image">
                            <img class="img-responsive img-services" src="public/img/services/Tax Audit.jpg">
                        </div>

                        <div class="card-content">
                            <h4 class="title-card">Tax Audit, Objection and Appeal</h4>
                            <p class="text-justify">
                                We give services to accompany tax payer on the process of Tax Audit, to submit objection on tax assessment letter, and to
                                file as well as to appeal to the tax court. Our objective is to help tax payer gets through
                                all the process according to tax procedures.
                            </p>
                            <!-- <p class="text-justify">
                          Our services to accompany tax payer on the process of Tax Audit and also.... <a href="#" data-toggle="modal" data-target="#taxtAudit">Readmore</a>
                        </p> -->
                        </div>
                    </div>
                </div>
                <div class="margin-services" style="">
                    <div class="col-md-4 col-md-offset-2 col-sm-12">
                        <div class="card">
                            <div class="card-image">
                                <img class="img-responsive img-services" src="public/img/services/Tax Review.jpg">
                            </div>

                            <div class="card-content">
                                <h4 class="title-card">Tax Review</h4>
                                <p class="text-justify">
                                    Review and due diligence on tax obligation, as usual business conduct or on acquisition purposes, to make sure that all the
                                    taxes have been complied with tax regulation
                                </p>
                                <!-- <p class="text-justify">
                            Review and due diligence on tax obligation, as usual business conduct.... <a href="#" data-toggle="modal" data-target="#taxReview">Readmore</a>
                          </p> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-image">
                                <img class="img-responsive img-services" src="public/img/services/Tax Planning.jpg">
                            </div>

                            <div class="card-content">
                                <h4 class="title-card">Tax Planning</h4>
                                <p class="text-justify">
                                    Design and review on tax strategy and plan to ensure tax payer is able to apply tax regulation in the most efficient way
                                    without violating any tax regulation
                                </p>
                                <!-- <p class="text-justify">
                            Design and review on tax strategy and plan to ensure tax payer able to apply.... <a href="#" data-toggle="modal" data-target="#taxPlanning">Readmore</a>
                          </p> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
      include 'item/js.php'; 
      include 'item/footer.php';
    ?>
</body>

</html>